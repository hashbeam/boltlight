# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Functional tests starting point.

These tests mock calls to the nodes but not internal ones.
"""


class FakeRpcError(Exception):
    """Simulate a fake RpcError."""


class FakeContext():  # pylint: disable=too-few-public-methods
    """Simulate a gRPC server context in order to (re)define abort().

    This allows checking connection to node before a context is available from
    a client request.
    """
    @staticmethod
    def abort(scode, msg):
        """Raise a runtime error."""
        exc = FakeRpcError(msg)
        exc.code = scode
        exc.message = msg
        raise exc from None

    @staticmethod
    def time_remaining():
        """Act as no timeout has been set by client."""
        return None
