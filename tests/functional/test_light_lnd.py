# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for light_lnd module."""

from binascii import hexlify, unhexlify
from importlib import import_module
from re import sub
from unittest import TestCase
from unittest.mock import patch

from grpc import StatusCode
from lnd_proto import lightning_pb2 as ln

from .. import PROJ_ROOT
from ..unit import fixtures_lnd as fix
from . import FakeContext, FakeRpcError

# pylint: disable=invalid-name, protected-access

CTX = 'context'
Enf = getattr(import_module(PROJ_ROOT + '.utils.bitcoin'), 'Enforcer')
errors = import_module(PROJ_ROOT + '.errors')
ERRORS = errors.ERRORS
pb = import_module(PROJ_ROOT + '.boltlight_pb2')
sett = import_module(PROJ_ROOT + '.settings')

sett.IMPLEMENTATION = 'lnd'
MOD = import_module(PROJ_ROOT + '.light_lnd')


class LightLndTests(TestCase):
    """Tests for light_lnd module."""

    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_BalanceOffChain(self, mocked_connect):
        stub = mocked_connect.return_value.__enter__.return_value
        stub.ListChannels.return_value = fix.LISTCHANNELRESPONSE
        out_tot = out_tot_now = out_max_now = in_tot = in_tot_now = \
            in_max_now = 0
        for chan in fix.LISTCHANNELRESPONSE.channels:
            out_tot += chan.local_balance
            if chan.initiator:
                out_tot += chan.commit_fee
        req = pb.BalanceOffChainRequest()
        res = MOD.BalanceOffChain(req, FakeContext())
        self.assertEqual(res.out_tot_msat, out_tot * 1000)

    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_BalanceOnChain(self, mocked_connect):
        stub = mocked_connect.return_value.__enter__.return_value
        stub.WalletBalance.return_value = fix.WALLETBALANCERESPONSE
        req = pb.BalanceOnChainRequest()
        res = MOD.BalanceOnChain(req, FakeContext())
        self.assertEqual(res.total_sat,
                         fix.WALLETBALANCERESPONSE.total_balance)
        self.assertEqual(res.confirmed_sat,
                         fix.WALLETBALANCERESPONSE.confirmed_balance)

    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_PayOnChain(self, mocked_connect):
        stub = mocked_connect.return_value.__enter__.return_value
        stub.SendCoins.return_value = fix.SENDCOINSRESPONSE
        req = pb.PayOnChainRequest(address='an_address',
                                   amount_sat=666,
                                   fee_sat_byte=1)
        res = MOD.PayOnChain(req, FakeContext())
        self.assertEqual(res.txid, fix.SENDCOINSRESPONSE.txid)

    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_OpenChannel(self, mocked_connect):
        stub = mocked_connect.return_value.__enter__.return_value
        stub.ConnectPeer.return_value = fix.CONNECTPEERRESPONSE
        stub.OpenChannelSync.return_value = fix.CHANNELPOINT
        funding_sat = 666000
        push_msat = 5000
        req = pb.OpenChannelRequest(funding_sat=funding_sat,
                                    node_uri=fix.NODE_URI,
                                    push_msat=push_msat,
                                    private=False)
        ctx = FakeContext()
        res = MOD.OpenChannel(req, ctx)
        self.assertEqual(res.funding_txid, fix.TXID)
        node_req = ln.ConnectPeerRequest(addr=fix.LIGHTNINGADDRESS, perm=True)
        stub.ConnectPeer.assert_called_once_with(node_req,
                                                 timeout=sett.IMPL_MIN_TIMEOUT)
        node_req = ln.OpenChannelRequest(node_pubkey=unhexlify(fix.NODE_ID),
                                         private=False,
                                         local_funding_amount=funding_sat,
                                         push_sat=int(push_msat / 1000))
        stub.OpenChannelSync.assert_called_once_with(
            node_req, timeout=sett.IMPL_MIN_TIMEOUT)
        # precision error case
        push_msat = 5001
        req = pb.OpenChannelRequest(funding_sat=funding_sat,
                                    node_uri=fix.NODE_URI,
                                    push_msat=push_msat,
                                    private=False)
        ctx = FakeContext()
        with self.assertRaises(FakeRpcError) as context:
            MOD.OpenChannel(req, ctx)
        self.assertEqual(context.exception.code, StatusCode.INVALID_ARGUMENT)

    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_CreateInvoice(self, mocked_connect):
        stub = mocked_connect.return_value.__enter__.return_value
        stub.AddInvoice.return_value = fix.ADDINVOICERESPONSE
        stub.LookupInvoice.return_value = fix.INVOICE_PENDING
        amt = 777000
        cltv = 7
        memo = 'a_description'
        exp = 500
        addr = 'an_address'
        req = pb.CreateInvoiceRequest(amount_msat=amt,
                                      description=memo,
                                      expiry=exp,
                                      min_final_cltv_expiry=cltv,
                                      fallback_addr=addr)
        ctx = FakeContext()
        res = MOD.CreateInvoice(req, ctx)
        self.assertEqual(res.payment_request,
                         fix.ADDINVOICERESPONSE.payment_request)
        self.assertEqual(res.payment_hash,
                         hexlify(fix.ADDINVOICERESPONSE.r_hash).decode())
        self.assertEqual(
            res.expires_at,
            fix.INVOICE_PENDING.creation_date + fix.INVOICE_PENDING.expiry)
        node_req = ln.Invoice(memo=memo,
                              expiry=exp,
                              fallback_addr=addr,
                              cltv_expiry=cltv,
                              value_msat=amt)
        stub.AddInvoice.assert_called_once_with(node_req,
                                                timeout=sett.IMPL_MIN_TIMEOUT)
        node_req = ln.PaymentHash(r_hash=fix.PAYMENT_RHASH)
        stub.LookupInvoice.assert_called_once_with(
            node_req, timeout=sett.IMPL_MIN_TIMEOUT)
        # invalid min_final_cltv_expiry parameter case
        req.min_final_cltv_expiry = Enf.MIN_FINAL_CLTV_EXPIRY['max_value'] + 1
        with self.assertRaises(FakeRpcError) as context:
            MOD.CreateInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.OUT_OF_RANGE)
        self.assertEqual(context.exception.message,
                         "Value '500000001' exceeds maximum treshold")

    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_DecodeInvoice(self, mocked_connect):
        stub = mocked_connect.return_value.__enter__.return_value
        stub.DecodePayReq.return_value = fix.PAYREQ
        req = pb.DecodeInvoiceRequest(payment_request='a_payment_request')
        res = MOD.DecodeInvoice(req, FakeContext())
        self.assertEqual(res.amount_msat, fix.PAYREQ.num_satoshis * 1000)
        node_req = ln.PayReqString(pay_req=req.payment_request)
        stub.DecodePayReq.assert_called_once_with(
            node_req, timeout=sett.IMPL_MIN_TIMEOUT)
        # unimplemented description error case
        req = pb.DecodeInvoiceRequest(payment_request='a_payment_request',
                                      description='a_description')
        with self.assertRaises(FakeRpcError) as context:
            MOD.DecodeInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'description',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)

    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_PayInvoice(self, mocked_connect):
        stub = mocked_connect.return_value.__enter__.return_value
        stub.SendPaymentSync.return_value = fix.SENDRESPONSE
        amt = 500
        memo = 'description'
        cltv = 7
        pay_req = 'a_payment_request'
        req = pb.PayInvoiceRequest(payment_request=pay_req,
                                   amount_msat=amt,
                                   description=memo,
                                   cltv_expiry_delta=cltv)
        res = MOD.PayInvoice(req, FakeContext())
        self.assertEqual(res.payment_preimage,
                         hexlify(fix.SENDRESPONSE.payment_preimage).decode())
        node_req = ln.SendRequest(payment_request=pay_req,
                                  final_cltv_delta=cltv,
                                  amt_msat=amt)
        stub.SendPaymentSync.assert_called_once_with(
            node_req, timeout=sett.IMPL_MIN_TIMEOUT)
