# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for light_eclair module."""

from importlib import import_module
from re import sub
from unittest import TestCase
from unittest.mock import patch

from grpc import StatusCode

from .. import PROJ_ROOT
from ..unit import fixtures_eclair as fix
from . import FakeContext, FakeRpcError

# pylint: disable=invalid-name, protected-access

CTX = 'context'
errors = import_module(PROJ_ROOT + '.errors')
ERRORS = errors.ERRORS
pb = import_module(PROJ_ROOT + '.boltlight_pb2')

MOD = import_module(PROJ_ROOT + '.light_eclair')


class LightEclairTests(TestCase):
    """Tests for light_eclair module."""

    @patch(MOD.__name__ + '.EclairRPC')
    def test_BalanceOffChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.channels.return_value = fix.CHANNELS, False
        out_tot = out_tot_now = out_max_now = in_tot = in_tot_now = \
            in_max_now = 0
        for chan in fix.CHANNELS:
            state = MOD._get_channel_state(chan)
            if state != pb.Channel.OPEN:
                continue
            out_tot += int(
                chan['data']['commitments']['localCommit']['spec']['toLocal'])
        req = pb.BalanceOffChainRequest()
        res = MOD.BalanceOffChain(req, FakeContext())
        self.assertEqual(res.out_tot_msat, out_tot)

    @patch(MOD.__name__ + '.EclairRPC')
    def test_BalanceOnChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.onchainbalance.return_value = fix.ONCHAINBALANCE, False
        req = pb.BalanceOnChainRequest()
        res = MOD.BalanceOnChain(req, FakeContext())
        conf = fix.ONCHAINBALANCE['confirmed']
        self.assertEqual(res.total_sat,
                         conf + fix.ONCHAINBALANCE['unconfirmed'])
        self.assertEqual(res.confirmed_sat, conf)

    @patch(MOD.__name__ + '.EclairRPC')
    def test_PayOnChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.sendonchain.return_value = fix.SENDONCHAIN, False
        # success case
        req = pb.PayOnChainRequest(address='an_address', amount_sat=666)
        res = MOD.PayOnChain(req, FakeContext())
        self.assertEqual(res.txid, fix.SENDONCHAIN)
        # request error case
        req = pb.PayOnChainRequest(address='an_address',
                                   amount_sat=666,
                                   fee_sat_byte=1)
        with self.assertRaises(FakeRpcError) as context:
            MOD.PayOnChain(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'fee_sat_byte',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)

    @patch(MOD.__name__ + '.EclairRPC')
    def test_OpenChannel(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.connect.return_value = fix.CONNECT, False
        ses.open.return_value = fix.OPEN, False
        ses.channel.return_value = fix.CHANNEL_NORMAL, False
        req = pb.OpenChannelRequest(funding_sat=666000,
                                    node_uri=fix.NODE_URI,
                                    push_msat=5000,
                                    private=False)
        ctx = FakeContext()
        res = MOD.OpenChannel(req, ctx)
        self.assertEqual(
            res.funding_txid, fix.CHANNEL_NORMAL['data']['commitments']
            ['commitInput']['outPoint'].split(':')[0])
        node_req = {'uri': fix.NODE_URI}
        ses.connect.assert_called_once_with(ctx, node_req)
        node_req = {
            'nodeId': fix.NODE_ID,
            'fundingSatoshis': req.funding_sat,
            'pushMsat': req.push_msat,
            'announceChannel': True,
        }
        ses.open.assert_called_once_with(ctx, node_req)
        node_req = {'channelId': fix.OPEN.split(' ')[2]}
        ses.channel.assert_called_once_with(ctx, node_req)

    @patch(MOD.__name__ + '.EclairRPC')
    def test_CreateInvoice(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.createinvoice.return_value = fix.CREATEINVOICE, False
        req = pb.CreateInvoiceRequest(amount_msat=333,
                                      description='a_description',
                                      expiry=500,
                                      fallback_addr='an_address')
        ctx = FakeContext()
        res = MOD.CreateInvoice(req, ctx)
        self.assertEqual(res.payment_request, fix.CREATEINVOICE['serialized'])
        self.assertEqual(res.payment_hash, fix.CREATEINVOICE['paymentHash'])
        self.assertEqual(
            res.expires_at,
            fix.CREATEINVOICE['timestamp'] + fix.CREATEINVOICE['expiry'])
        node_req = {
            'description': req.description,
            'amountMsat': req.amount_msat,
            'expireIn': req.expiry,
            'fallbackAddress': req.fallback_addr
        }
        ses.createinvoice.assert_called_once_with(ctx, node_req)
        # unimplemented parameter error case
        req = pb.CreateInvoiceRequest(amount_msat=333,
                                      description='a_description',
                                      expiry=500,
                                      min_final_cltv_expiry=7,
                                      fallback_addr='an_address')
        with self.assertRaises(FakeRpcError) as context:
            MOD.CreateInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'min_final_cltv_expiry',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)

    @patch(MOD.__name__ + '.EclairRPC')
    def test_DecodeInvoice(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.parseinvoice.return_value = fix.PARSEINVOICE_D_HASH, False
        req = pb.DecodeInvoiceRequest(payment_request='a_payment_request')
        ctx = FakeContext()
        res = MOD.DecodeInvoice(req, ctx)
        self.assertEqual(res.amount_msat,
                         int(fix.PARSEINVOICE_D_HASH['amount']))
        node_req = {'invoice': req.payment_request}
        ses.parseinvoice.assert_called_once_with(ctx, node_req)
        # unimplemented parameter error case
        req = pb.DecodeInvoiceRequest(payment_request='a_payment_request',
                                      description='a_description')
        with self.assertRaises(FakeRpcError) as context:
            MOD.DecodeInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'description',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)

    @patch(MOD.__name__ + '.EclairRPC')
    def test_PayInvoice(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.payinvoice.return_value = fix.PAYINVOICE, False
        req = pb.PayInvoiceRequest(
            payment_request='a_payment_request_with_no_amount_encoded',
            description='a_description',
            amount_msat=500)
        ctx = FakeContext()
        res = MOD.PayInvoice(req, ctx)
        self.assertEqual(res.payment_preimage,
                         fix.PAYINVOICE['paymentPreimage'])
        node_req = {
            'invoice': req.payment_request,
            'blocking': True,
            'amountMsat': req.amount_msat
        }
        ses.payinvoice.assert_called_once_with(ctx, node_req)
        # unimplemented parameter error case
        req.cltv_expiry_delta = 6
        with self.assertRaises(FakeRpcError) as context:
            MOD.PayInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'cltv_expiry_delta',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)
