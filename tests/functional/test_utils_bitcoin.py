# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for utils.bitcoin module."""

from importlib import import_module
from unittest import TestCase

from .. import PROJ_ROOT

# pylint: disable=invalid-name, protected-access

CTX = 'context'
Enf = getattr(import_module(PROJ_ROOT + '.utils.bitcoin'), 'Enforcer')

MOD = import_module(PROJ_ROOT + '.utils.bitcoin')


class UtilsBitcoinTests(TestCase):
    def test_conversion(self):
        # 1. Correct cases
        # msats to sats
        res = MOD.convert(CTX, Enf.MSATS, Enf.SATS, 777000, Enf.MSATS)
        self.assertEqual(res, 777)
        # bits to btc
        res = MOD.convert(CTX,
                          Enf.BITS,
                          Enf.BTC,
                          777000,
                          Enf.SATS,
                          enforce=Enf.OC_TX)
        self.assertEqual(res, 0.777000)
        # btc to sats
        res = MOD.convert(CTX, Enf.BTC, Enf.SATS, 0.777, Enf.BITS)
        self.assertEqual(res, 77700000)
        # sats to msats (input value is a string)
        res = MOD.convert(CTX,
                          Enf.BITS,
                          Enf.MSATS,
                          '7.00777',
                          Enf.MSATS,
                          enforce=Enf.LN_PAYREQ)
        self.assertEqual(res, 700777)
        # 2. Error cases
        # bits to sats, lost precision
        with self.assertRaises(Exception):
            res = MOD.convert(CTX,
                              Enf.BITS,
                              Enf.SATS,
                              0.009,
                              Enf.SATS,
                              enforce=Enf.SATS)
        # value is not a number
        with self.assertRaises(Exception):
            res = MOD.convert(CTX, Enf.SATS, Enf.MSATS, 'notanumber',
                              Enf.MSATS)
        # value exceeds maximum precision
        with self.assertRaises(Exception):
            res = MOD.convert(CTX,
                              Enf.BITS,
                              Enf.MSATS,
                              7.007771,
                              Enf.MSATS,
                              enforce=Enf.LN_PAYREQ)
