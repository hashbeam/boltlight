# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for light_electrum module."""

from importlib import import_module
from re import sub
from unittest import TestCase
from unittest.mock import patch

from grpc import StatusCode

from .. import PROJ_ROOT
from ..unit import fixtures_electrum as fix
from . import FakeContext, FakeRpcError

# pylint: disable=invalid-name, protected-access

CTX = 'context'
errors = import_module(PROJ_ROOT + '.errors')
ERRORS = errors.ERRORS
pb = import_module(PROJ_ROOT + '.boltlight_pb2')
sett = import_module(PROJ_ROOT + '.settings')

sett.IMPLEMENTATION = 'electrum'
MOD = import_module(PROJ_ROOT + '.light_electrum')


class LightElectrumTests(TestCase):
    """Tests for light_electrum module."""
    @patch(MOD.__name__ + '.ElectrumRPC')
    def test_BalanceOffChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.list_channels.return_value = fix.LIST_CHANNELS, False
        out_tot = out_tot_now = out_max_now = in_tot = in_tot_now = \
            in_max_now = 0
        for chan in fix.LIST_CHANNELS:
            state = MOD._get_channel_state(chan)
            if state != pb.Channel.OPEN:
                continue
            out_tot += int(chan['local_balance'])
        req = pb.BalanceOffChainRequest()
        res = MOD.BalanceOffChain(req, FakeContext())
        self.assertEqual(res.out_tot_msat, out_tot * 1000)

    @patch(MOD.__name__ + '.ElectrumRPC')
    def test_BalanceOnChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.getbalance.return_value = fix.GETBALANCE, False
        req = pb.BalanceOnChainRequest()
        res = MOD.BalanceOnChain(req, FakeContext())
        conf = float(fix.GETBALANCE['confirmed'])
        tot = conf + float(fix.GETBALANCE['unconfirmed'])
        self.assertEqual(res.total_sat, tot * 10**8)
        self.assertEqual(res.confirmed_sat, conf * 10**8)

    @patch(MOD.__name__ + '.ElectrumRPC')
    def test_PayOnChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.payto.return_value = fix.PAYTO, False
        ses.broadcast.return_value = fix.BROADCAST, False
        req = pb.PayOnChainRequest(address='an_address',
                                   amount_sat=666,
                                   fee_sat_byte=1)
        res = MOD.PayOnChain(req, FakeContext())
        self.assertEqual(res.txid, fix.BROADCAST)

    @patch(MOD.__name__ + '.ElectrumRPC')
    def test_OpenChannel(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.open_channel.return_value = fix.OPEN_CHANNEL, False
        req = pb.OpenChannelRequest(funding_sat=666000,
                                    node_uri=fix.NODE_URI,
                                    push_msat=5000,
                                    private=True)
        ctx = FakeContext()
        res = MOD.OpenChannel(req, ctx)
        self.assertEqual(res.funding_txid, fix.OPEN_CHANNEL.split(':')[0])
        node_req = {
            'connection_string': fix.NODE_URI,
            'amount': req.funding_sat / 1e8,
            'push_amount': req.push_msat / 1e11,
        }
        ses.open_channel.assert_called_once_with(ctx, node_req)

    @patch(MOD.__name__ + '.ElectrumRPC')
    def test_CreateInvoice(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.add_lightning_request.return_value = fix.ADD_LIGHTNING_REQUEST, False
        req = pb.CreateInvoiceRequest(amount_msat=600000,
                                      description='a_description',
                                      expiry=500)
        ctx = FakeContext()
        res = MOD.CreateInvoice(req, ctx)
        self.assertEqual(res.payment_request,
                         fix.ADD_LIGHTNING_REQUEST['invoice'])
        self.assertEqual(res.payment_hash, fix.ADD_LIGHTNING_REQUEST['rhash'])
        self.assertEqual(
            res.expires_at,
            fix.ADD_LIGHTNING_REQUEST['timestamp'] + \
            fix.ADD_LIGHTNING_REQUEST['expiration'])
        node_req = {
            'amount': 6e-06,
            'expiration': 500,
            'memo': 'a_description'
        }
        ses.add_lightning_request.assert_called_once_with(ctx, node_req)
        # unimplemented parameter error case
        req = pb.CreateInvoiceRequest(amount_msat=600000,
                                      description='a_description',
                                      expiry=500,
                                      min_final_cltv_expiry=7)
        with self.assertRaises(FakeRpcError) as context:
            MOD.CreateInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'min_final_cltv_expiry',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)

    @patch(MOD.__name__ + '.ElectrumRPC')
    def test_DecodeInvoice(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.decode_invoice.return_value = fix.DECODE_INVOICE, False
        req = pb.DecodeInvoiceRequest(payment_request='a_payment_request')
        ctx = FakeContext()
        res = MOD.DecodeInvoice(req, ctx)
        self.assertEqual(res.amount_msat,
                         int(fix.DECODE_INVOICE['amount_msat']))
        node_req = {'invoice': req.payment_request}
        ses.decode_invoice.assert_called_once_with(ctx, node_req)
        # unimplemented parameter error case
        req = pb.DecodeInvoiceRequest(payment_request='a_payment_request',
                                      description='a_description')
        with self.assertRaises(FakeRpcError) as context:
            MOD.DecodeInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'description',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)

    @patch(MOD.__name__ + '.ElectrumRPC')
    def test_PayInvoice(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.lnpay.return_value = fix.LNPAY_SUCCESS, False
        req = pb.PayInvoiceRequest(
            payment_request=fix.ADD_LIGHTNING_REQUEST['invoice'])
        ctx = FakeContext()
        res = MOD.PayInvoice(req, ctx)
        self.assertEqual(res.payment_preimage, fix.LNPAY_SUCCESS['preimage'])
        node_req = {
            'invoice': req.payment_request,
        }
        ses.lnpay.assert_called_once_with(ctx, node_req)
        # unimplemented parameter error case
        req.cltv_expiry_delta = 6
        with self.assertRaises(FakeRpcError) as context:
            MOD.PayInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'cltv_expiry_delta',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)
