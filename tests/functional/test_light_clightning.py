# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for light_clightning module."""

from importlib import import_module
from re import sub
from unittest import TestCase
from unittest.mock import patch

from grpc import StatusCode

from .. import PROJ_ROOT
from ..unit import fixtures_clightning as fix
from . import FakeContext, FakeRpcError

# pylint: disable=invalid-name, protected-access

CTX = 'context'
errors = import_module(PROJ_ROOT + '.errors')
ERRORS = errors.ERRORS
pb = import_module(PROJ_ROOT + '.boltlight_pb2')

MOD = import_module(PROJ_ROOT + '.light_clightning')


class LightClightningTests(TestCase):
    """Tests for light_clightning module."""
    @patch(MOD.__name__ + '.ClightningRPC')
    def test_BalanceOffChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.listpeers.return_value = fix.LISTPEERS, False
        out_tot = out_tot_now = out_max_now = in_tot = in_tot_now = \
            in_max_now = 0
        for peer in fix.LISTPEERS['peers']:
            for chan in peer['channels']:
                state = MOD._get_channel_state(chan)
                if state != pb.Channel.OPEN:
                    continue
                out_tot += int(chan.get('msatoshi_to_us', 0))
        req = pb.BalanceOffChainRequest()
        res = MOD.BalanceOffChain(req, FakeContext())
        self.assertEqual(res.out_tot_msat, out_tot)

    @patch(MOD.__name__ + '.ClightningRPC')
    def test_BalanceOnChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.listfunds.return_value = fix.LISTFUNDS, False
        req = pb.BalanceOnChainRequest()
        res = MOD.BalanceOnChain(req, FakeContext())
        tot = conf = 0
        for output in fix.LISTFUNDS['outputs']:
            tot += output['value']
            if output['status'] == 'confirmed':
                conf += output['value']
        self.assertEqual(res.total_sat, tot)
        self.assertEqual(res.confirmed_sat, conf)

    @patch(MOD.__name__ + '.ClightningRPC')
    def test_PayOnChain(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.withdraw.return_value = fix.WITHDRAW, False
        req = pb.PayOnChainRequest(address='an_address',
                                   amount_sat=666,
                                   fee_sat_byte=1)
        ctx = FakeContext()
        res = MOD.PayOnChain(req, ctx)
        self.assertEqual(res.txid, fix.WITHDRAW['txid'])
        fee_sat_byte = req.fee_sat_byte * 1000
        node_req = {
            'destination': req.address,
            'satoshi': req.amount_sat,
            'feerate': f'{fee_sat_byte}perkb'
        }
        ses.withdraw.assert_called_once_with(ctx, node_req)

    @patch(MOD.__name__ + '.ClightningRPC')
    def test_OpenChannel(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.connect.return_value = fix.CONNECT, False
        ses.fundchannel.return_value = fix.FUNDCHANNEL, False
        req = pb.OpenChannelRequest(funding_sat=666000,
                                    node_uri=fix.NODE_URI,
                                    push_msat=5000,
                                    private=False)
        ctx = FakeContext()
        res = MOD.OpenChannel(req, ctx)
        self.assertEqual(res.funding_txid, fix.FUNDCHANNEL['txid'])
        node_req = {'peer_id': fix.NODE_URI}
        ses.connect.assert_called_once_with(ctx, node_req)
        node_req = {
            'node_id': fix.NODE_ID,
            'amount': req.funding_sat,
            'push_msat': req.push_msat
        }
        ses.fundchannel.assert_called_once_with(ctx, node_req)

    @patch(MOD.__name__ + '.datetime', autospec=True)
    @patch(MOD.__name__ + '.ClightningRPC')
    def test_CreateInvoice(self, mocked_rpcses, mocked_datetime):
        ses = mocked_rpcses.return_value
        ses.invoice.return_value = fix.INVOICE, False
        now = 1533152937.911157
        mocked_datetime.now().timestamp.return_value = now
        req = pb.CreateInvoiceRequest(amount_msat=333,
                                      description='a_description',
                                      expiry=500,
                                      min_final_cltv_expiry=7,
                                      fallback_addr='an_address')
        ctx = FakeContext()
        res = MOD.CreateInvoice(req, ctx)
        self.assertEqual(res.payment_request, fix.INVOICE['bolt11'])
        self.assertEqual(res.payment_hash, fix.INVOICE['payment_hash'])
        self.assertEqual(res.expires_at, fix.INVOICE['expires_at'])
        label = str(int(now * 1e6))
        node_req = {
            'cltv': req.min_final_cltv_expiry,
            'msatoshi': req.amount_msat,
            'description': req.description,
            'label': label,
            'expiry': req.expiry,
            'fallbacks': [req.fallback_addr]
        }
        ses.invoice.assert_called_once_with(ctx, node_req)

    @patch(MOD.__name__ + '.ClightningRPC')
    def test_DecodeInvoice(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.decodepay.return_value = fix.DECODEPAY, False
        req = pb.DecodeInvoiceRequest(payment_request='a_payment_request',
                                      description='a_description')
        ctx = FakeContext()
        res = MOD.DecodeInvoice(req, ctx)
        self.assertEqual(res.amount_msat, int(fix.DECODEPAY['msatoshi']))
        node_req = {
            'bolt11': req.payment_request,
            'description': req.description
        }
        ses.decodepay.assert_called_once_with(ctx, node_req)

    @patch(MOD.__name__ + '.ClightningRPC')
    def test_PayInvoice(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        ses.pay.return_value = fix.PAY, False
        req = pb.PayInvoiceRequest(
            payment_request='a_payment_request_with_no_amount_encoded',
            amount_msat=500,
            cltv_expiry_delta=7)
        ctx = FakeContext()
        res = MOD.PayInvoice(req, ctx)
        self.assertEqual(res.payment_preimage, fix.PAY['payment_preimage'])
        node_req = {
            'bolt11': req.payment_request,
            'msatoshi': req.amount_msat,
            'maxdelay': req.cltv_expiry_delta
        }
        ses.pay.assert_called_once_with(ctx, node_req)
        # unimplemented parameter error case
        req = pb.PayInvoiceRequest(
            payment_request='a_payment_request_with_no_amount_encoded',
            description='a_description',
            amount_msat=500,
            cltv_expiry_delta=7)
        with self.assertRaises(FakeRpcError) as context:
            MOD.PayInvoice(req, FakeContext())
        self.assertEqual(context.exception.code, StatusCode.UNIMPLEMENTED)
        msg = sub('%PARAM%', 'description',
                  ERRORS['unimplemented_parameter']['msg'], 1)
        self.assertEqual(context.exception.message, msg)
