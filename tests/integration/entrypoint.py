# Boltlight - a LN node wrapper
#
# Copyright (C) 2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Integration tests entrypoint."""

import os
import shutil
import socket
import sys
from contextlib import suppress
from signal import SIGTERM, signal
from time import sleep

import docker
from bitcoin import SelectParams
from blitskrieg import blitskrieg_pb2 as bpb
from blitskrieg import blitskrieg_pb2_grpc as bpb_grpc
from blitskrieg import settings as blits_sett
from blitskrieg.utils.configure_stack import lines_to_list, list_to_file
from pytest import ExitCode
from pytest import main as run_pytest

from boltlight.utils.exceptions import InterruptException
from boltlight.utils.misc import die, handle_keyboardinterrupt, handle_sigterm

from . import settings as sett
from .utils import check_blits_connectivity, clean_environment, connect

signal(SIGTERM, handle_sigterm)

BLITS_CFG = """
[blitskrieg]
logs_level = debug
[bli]
[services]
"""


def _run_blitskrieg(client, implementations):
    """Run a blitskrieg container."""

    client.networks.create(sett.BLITS_NETWORK, driver='bridge')
    os.makedirs(sett.BLITS_DIR, exist_ok=True)
    config = lines_to_list(BLITS_CFG)
    for impl in implementations:
        ref = sett.SUPPORTED_IMPLEMENTATIONS[impl] or getattr(
            blits_sett, f'{impl.upper()}_REF')
        config += [f'{impl}_ref = {ref}']
    list_to_file(config, sett.BLITS_CONFIG)
    volumes = {
        sett.BLITS_DATA_DIR: {
            'bind': '/srv/app/data',
            'mode': 'rw'
        },
        sett.BLITS_CONFIG: {
            'bind': '/srv/app/.blitskrieg/config',
            'mode': 'ro'
        },
    }
    if os.path.exists(sett.DOCKER_SOCK):
        volumes[sett.DOCKER_SOCK] = {'bind': sett.DOCKER_SOCK, 'mode': 'rw'}
    dock_env = {k: v for k, v in os.environ.items() if k.startswith('DOCKER_')}
    env = {'HOST_WORKDIR': sett.INTEGRATION_TESTS_DIR} | dock_env
    if os.path.exists('/.dockerenv') and env.get(
            'DOCKER_HOST', '').startswith('tcp://docker:'):
        docker_ip = socket.gethostbyname('docker')
        docker_port = env['DOCKER_HOST'].split(':')[-1]
        env['DOCKER_HOST'] = f'tcp://{docker_ip}:{docker_port}'
    client.containers.run(
        sett.BLITS_IMG,
        detach=True,
        ports={f'{sett.BLITS_PORT}/tcp': sett.BLITS_PORT},
        name=sett.BLITS_NAME,
        environment=env,
        volumes=volumes,
        network=sett.BLITS_NETWORK,
    )


@handle_keyboardinterrupt
def _start_tests(args):
    """Start integration tests.

    Select tests to be ran, build a docker image of the boltlight to be tested,
    run blitskrieg if needed and check connectivity with it, run tests, clean
    the test environment and finally exit with the code returned by pytest.
    """
    implementations = []
    if args:
        for arg in args:
            if not arg:
                continue
            if arg in sett.SUPPORTED_IMPLEMENTATIONS:
                implementations.append(arg)
            else:
                die(f'invalid implementation "{arg}". valid values: '
                    f'{", ".join(sett.SUPPORTED_IMPLEMENTATIONS)}')
    implementations = implementations or sett.SUPPORTED_IMPLEMENTATIONS
    tests = []
    for impl in implementations:
        tests.append(f'test_{impl}.py')

    print('will test the following implementations:',
          ', '.join(implementations))

    client = docker.from_env()

    launched_blits = False
    try:
        print(f'looking for a running blitskrieg at {sett.BLITS_URL}...')
        with connect(sett.BLITS_URL, bpb_grpc, 'BlitskriegStub',
                     timeout=5) as stub:
            res = stub.GetInfo(bpb.GetInfoRequest())
            print(f'found blitskrieg version {res.version}')
    except Exception:  # pylint: disable=broad-except
        print('could not find a running blitskrieg, running it...', end='')
        _run_blitskrieg(client, implementations)
        launched_blits = True
        print('done')

    print('building boltlight image (could take a while)... ', end='')
    client.images.build(path='.', tag=sett.BOLTLIGHT_IMG, rm=True)
    print('done')

    if os.path.exists('/.dockerenv'):
        sett.BLITS_URL = f'docker:{sett.BLITS_PORT}'
        sett.BOLTLIGHT_URL = f'docker:{sett.BOLTLIGHT_PORT}'

    clean_environment()
    os.makedirs(sett.BOLTLIGHT_DIR)
    if os.path.exists('/.dockerenv'):  # assuming run in CI
        shutil.chown(sett.BOLTLIGHT_DIR, user=sett.CI_UID)
    check_blits_connectivity()

    # initialize python-bitcoinlib with the correct chain
    SelectParams(sett.BITCOIN_NETWORK)

    print('starting integration tests...')
    exit_code = run_pytest(
        ['-vrP'] +
        [os.path.join(sett.INTEGRATION_TESTS_DIR, test) for test in tests])

    clean_environment()
    if launched_blits:
        blits = client.containers.get(sett.BLITS_NAME)
        blits.stop()
        blits.remove(v=True, force=True)
        client.networks.get(sett.BLITS_NETWORK).remove()
    sys.exit(exit_code)


def start_tests(args):
    """Integration tests entrypoint.

    Start integration tests, eventually catching an InterruptException.
    """
    try:
        _start_tests(args)
    except InterruptException:
        with suppress(KeyboardInterrupt):
            # sleep time needs to be higher than tox suicide_timeout
            sleep(3)
        clean_environment()
        sys.exit(ExitCode.INTERRUPTED)
