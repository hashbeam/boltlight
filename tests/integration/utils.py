# Boltlight - a LN node wrapper
#
# Copyright (C) 2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Utilities for the integration tests."""

from contextlib import contextmanager, suppress
from pathlib import Path
from shutil import rmtree
from time import sleep

import docker
from blitskrieg import blitskrieg_pb2 as bpb
from blitskrieg import blitskrieg_pb2_grpc as bpb_grpc
from docker.errors import DockerException
from grpc import (
    FutureTimeoutError, RpcError, channel_ready_future, insecure_channel)
from pytest import ExitCode

from boltlight.utils.misc import die

from . import settings as sett


class TestInternalError(Exception):
    """Raised when an internal error occurs."""


def check_blits_connectivity():
    """Check that a compatible version of blitskrieg is running."""
    print(f'checking connectivity with blitskrieg at {sett.BLITS_URL}... ',
          end='')
    try:
        with connect(sett.BLITS_URL, bpb_grpc, 'BlitskriegStub',
                     timeout=5) as stub:
            res = stub.GetInfo(bpb.GetInfoRequest())
            if not res.version.startswith('0.'):
                die('blitskrieg version is incompatible')
    except TestInternalError:
        die('cannot find a running blitskrieg',
            exit_code=ExitCode.INTERNAL_ERROR)
    print('done')


def clean_environment():
    """Clean the test environment.

    Try to remove the tested container, its data and the stack created by
    blitskrieg.
    """
    print('cleaning test environment... ', end='')
    rmtree(sett.BOLTLIGHT_DIR, ignore_errors=True)
    remove_tested_container()
    with suppress(Exception):
        with connect(sett.BLITS_URL, bpb_grpc, 'BlitskriegStub') as stub:
            stub.RemoveStack(bpb.RemoveStackRequest())
    print('done')


def remove_tested_container():
    """Try to stop and remove the tested container."""
    client = docker.from_env()
    with suppress(DockerException):
        tested_boltlight = client.containers.get(sett.TESTED_BOLTLIGHT)
        tested_boltlight.stop()
        tested_boltlight.remove()


def call_with_attempts(api, request, attempts=3):
    """Call a gRPC API with the given request for a max of attempts times."""
    err_msg = f'call to {api._method.decode("utf-8")} failed'
    while attempts:
        try:
            return api(request)
        except RpcError as err:
            sleep(1)
            err_msg = err.details()
            attempts -= 1
    raise RpcError(err_msg)


@contextmanager
def connect(server, stub_module, stub_class, timeout=15):
    """Connect to an insecure gRPC server."""
    channel = None
    channel = insecure_channel(server)
    future_channel = channel_ready_future(channel)
    try:
        future_channel.result(timeout=timeout)
    except FutureTimeoutError:  # handle gRPC channel that did not connect
        raise TestInternalError(
            'Failed to dial server, call timed out') from None
    else:
        stub = getattr(stub_module, stub_class)(channel)
        yield stub
        channel.close()
