# Boltlight - a LN node wrapper
#
# Copyright (C) 2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Integration tests for c-lightning."""

from blitskrieg.settings import CLIGHTNING_REF

from . import IntegrationTestsCases
from . import settings as sett


class ClightningTests(IntegrationTestsCases.IntegrationTests):
    """Class to hold c-lightning specific tests.

    Common tests are defined in the parent class.
    """
    IMPLEMENTATION = 'clightning'
    HAS_PASSWORD = False
    SHARE_VOLUME = True
    ref = sett.SUPPORTED_IMPLEMENTATIONS[IMPLEMENTATION] or CLIGHTNING_REF
    VERSION = f'v{ref}'
