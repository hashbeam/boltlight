# Boltlight - a LN node wrapper
#
# Copyright (C) 2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Integration tests for lnd."""

from blitskrieg.settings import LND_REF

from . import IntegrationTestsCases
from . import settings as sett


class LndTests(IntegrationTestsCases.IntegrationTests):
    """Class to hold lnd specific tests.

    Common tests are defined in the parent class.
    """
    IMPLEMENTATION = 'lnd'
    HAS_PASSWORD = True
    SHARE_VOLUME = True
    ref = sett.SUPPORTED_IMPLEMENTATIONS[IMPLEMENTATION] or LND_REF
    VERSION = f'{ref} commit=v{ref}'
