# Boltlight - a LN node wrapper
#
# Copyright (C) 2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Integration tests for electrum."""

from blitskrieg.settings import ELECTRUM_REF
from grpc import RpcError, StatusCode

from . import IntegrationTestsCases
from . import settings as sett


class ElectrumTests(IntegrationTestsCases.IntegrationTests):
    """Class to hold electrum specific tests.

    Common tests are defined in the parent class.
    """
    IMPLEMENTATION = 'electrum'
    HAS_PASSWORD = True
    SHARE_VOLUME = False
    ref = sett.SUPPORTED_IMPLEMENTATIONS[IMPLEMENTATION] or ELECTRUM_REF
    VERSION = ref

    def test_GetNodeInfo(self):
        res = super().test_GetNodeInfo()
        assert res.node_uri

    def test_NewAddress_nested_segwit(self):
        with self.assertRaises(RpcError) as ctx:
            super().test_NewAddress_nested_segwit()
        self.assertEqual(ctx.exception.code(), StatusCode.UNIMPLEMENTED)
