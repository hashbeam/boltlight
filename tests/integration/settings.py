# Boltlight - a LN node wrapper
#
# Copyright (C) 2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Settings for the integration tests."""

from pathlib import Path

PROJ_ROOT = Path(Path(__file__).parents[1], 'boltlight').name
SUPPORTED_IMPLEMENTATIONS = {
    'clightning': '0.10.2',
    'eclair': '0.7.0',
    'electrum': '4.2.1',
    'lnd': '0.14.3-beta'
}
TESTED_BOLTLIGHT = 'tested_boltlight'
INTEGRATION_TESTS_DIR = str(Path(__file__).parents[0])
BOLTLIGHT_DIR = str(Path(INTEGRATION_TESTS_DIR, 'boltlight_tmp').resolve())
BOLTLIGHT_IMG = 'boltlight:tested'
BOLTLIGHT_PORT = 1708
BOLTLIGHT_URL = f'localhost:{BOLTLIGHT_PORT}'
BLITS_VER = '0.2.1'
BLITS_IMG = f'registry.gitlab.com/hashbeam/blitskrieg:{BLITS_VER}'
BLITS_NETWORK = 'blits_default'
BLITS_NAME = 'blits_boltlight'
BLITS_PORT = 6969
BLITS_URL = f'localhost:{BLITS_PORT}'
BLITS_DIR = str(Path(INTEGRATION_TESTS_DIR, 'blitskrieg_tmp').resolve())
BLITS_CONFIG = str(Path(BLITS_DIR, 'config').resolve())
BLITS_DATA_DIR = str(Path(INTEGRATION_TESTS_DIR, 'data').resolve())
BOLTLIGHT_DATA = '/.boltlight'
BITCOIN_NETWORK = 'regtest'
DOCKER_SOCK = '/var/run/docker.sock'
CI_UID = 1000
