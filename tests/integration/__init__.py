# Boltlight - a LN node wrapper
#
# Copyright (C) 2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Integration tests module."""

import os
from time import sleep
from unittest import TestCase

import docker
from bitcoin.wallet import P2SHBitcoinAddress, P2WPKHBitcoinAddress
from blitskrieg import blitskrieg_pb2 as bpb
from blitskrieg import blitskrieg_pb2_grpc as bpb_grpc
from blitskrieg.settings import PASSWORD
from blitskrieg.utils import configure_stack
from blitskrieg.utils.configure_stack import (
    BOLTLIGHT_CFG, lines_to_list, list_to_file)
from boltlight_proto import boltlight_pb2 as pb
from boltlight_proto import boltlight_pb2_grpc as pb_grpc
from grpc import RpcError

from . import settings as sett
from .utils import call_with_attempts, connect, remove_tested_container


class IntegrationTestsCases:
    """Wrapper class to prevent tests to be executed twice.

    unittest only runs module-level classes that inherit from TestCase.
    """

    class IntegrationTests(TestCase):
        """Common integration tests"""

        @classmethod
        def setUpClass(cls):
            """Prepare tests environment.

            Create a stack on blitskrieg with the LN node under testing and
            configure, run and unlock the tested boltlight to test the node.
            """
            client = docker.from_env()

            impl = cls.IMPLEMENTATION  # pylint: disable=no-member
            kwargs = {impl: 1}
            with connect(sett.BLITS_URL, bpb_grpc, 'BlitskriegStub') as stub:
                stub.CreateStack(bpb.CreateStackRequest(**kwargs))

            config = lines_to_list(BOLTLIGHT_CFG.format(impl))
            config.insert(1, 'logs_level = debug')
            node = 1
            node_cfg = getattr(configure_stack,
                               f'BOLTLIGHT_{impl.upper()}_CFG').format(node)
            config += lines_to_list(node_cfg)
            list_to_file(config, sett.BOLTLIGHT_DIR + '/config')

            boltlight_vol = {
                sett.BOLTLIGHT_DIR: {
                    'bind': sett.BOLTLIGHT_DATA,
                    'mode': 'rw'
                }
            }

            # run boltlight-secure
            passwords = 'boltlight_password={pwd} '
            if cls.HAS_PASSWORD:  # pylint: disable=no-member
                passwords += f'{impl}_password={{pwd}} '
            passwords = passwords.format(pwd=PASSWORD)
            cmd = ['-c', passwords + 'boltlight-secure']
            uid = os.getuid()
            if os.path.exists('/.dockerenv'):  # assuming run in CI
                uid = sett.CI_UID
            client.containers.run(sett.BOLTLIGHT_IMG,
                                  auto_remove=True,
                                  entrypoint='bash',
                                  volumes=boltlight_vol,
                                  user=uid,
                                  command=cmd)

            # run boltlight instance to be tested
            volumes = boltlight_vol
            if cls.SHARE_VOLUME:  # pylint: disable=no-member
                impl_vol = f'krieg_{impl}_data_{node}'
                volumes[impl_vol] = {'bind': '/srv/node/', 'mode': 'rw'}
            client.containers.run(
                sett.BOLTLIGHT_IMG,
                detach=True,
                network=sett.BLITS_NETWORK,
                ports={f'{sett.BOLTLIGHT_PORT}/tcp': sett.BOLTLIGHT_PORT},
                name=sett.TESTED_BOLTLIGHT,
                user=uid,
                volumes=volumes)

            # unlock boltlight instance to be tested
            with connect(sett.BOLTLIGHT_URL, pb_grpc, 'UnlockerStub') as stub:
                stub.Unlock(pb.UnlockRequest(password=PASSWORD,
                                             unlock_node=True),
                            wait_for_ready=True)
            sleep(1)

        @classmethod
        def tearDownClass(cls):
            """Clean environment after tests."""
            remove_tested_container()

        def test_GetInfo(self):
            """Test GetInfo API."""
            with connect(sett.BOLTLIGHT_URL, pb_grpc, 'BoltlightStub') as stub:
                res = call_with_attempts(stub.GetInfo, pb.GetInfoRequest())
                self.assertEqual(res.node_implementation, self.IMPLEMENTATION)
                self.assertEqual(res.node_version, self.VERSION)
                return res

        def test_GetNodeInfo(self):
            """Test GetNodeInfo API."""
            with connect(sett.BOLTLIGHT_URL, pb_grpc, 'LightningStub') as stub:
                res = call_with_attempts(stub.GetNodeInfo,
                                         pb.GetNodeInfoRequest())
                self.assertEqual(res.network, pb.Network.REGTEST)
                assert res.identity_pubkey
                return res

        def test_NewAddress_native_segwit(self):
            """Test NewAddress API with address type set to NATIVE_SEGWIT."""
            with connect(sett.BOLTLIGHT_URL, pb_grpc, 'LightningStub') as stub:
                res = stub.NewAddress(
                    pb.NewAddressRequest(addr_type=pb.Address.NATIVE_SEGWIT))
                assert res.address
                P2WPKHBitcoinAddress(res.address)

        def test_NewAddress_nested_segwit(self):
            """Test NewAddress API with address type set to NESTED_SEGWIT."""
            with connect(sett.BOLTLIGHT_URL, pb_grpc, 'LightningStub') as stub:
                res = stub.NewAddress(
                    pb.NewAddressRequest(addr_type=pb.Address.NESTED_SEGWIT))
                assert res.address
                P2SHBitcoinAddress(res.address)

        def test_BalanceOnChain(self):
            """Test BalanceOnChain API."""
            with connect(sett.BOLTLIGHT_URL, pb_grpc, 'LightningStub') as stub:
                res = stub.BalanceOnChain(pb.BalanceOnChainRequest())
                previous_balance = res.confirmed_sat
            with connect(sett.BLITS_URL, bpb_grpc, 'LightningStub') as stub:
                res = stub.FundNodes(bpb.FundNodesRequest())
            new_balance = previous_balance + res.sent_amount
            balance = previous_balance
            with connect(sett.BOLTLIGHT_URL, pb_grpc, 'LightningStub') as stub:
                # handle slowness of some nodes on seeing blockchain events
                attempts = 10
                while balance == previous_balance and attempts:
                    res = stub.BalanceOnChain(pb.BalanceOnChainRequest())
                    balance = res.confirmed_sat
                    attempts -= 1
                    sleep(3)
                self.assertEqual(balance, new_balance)
