# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for errors module."""

from importlib import import_module
from re import sub
from unittest import TestCase
from unittest.mock import Mock, patch

from grpc import StatusCode

from .. import PROJ_ROOT, reset_mocks

# pylint: disable=invalid-name

errors = import_module(PROJ_ROOT + '.errors')
ERRORS = getattr(errors, 'ERRORS')
sett = import_module(PROJ_ROOT + '.settings')
MOD = import_module(PROJ_ROOT + '.errors')


class ErrorsTests(TestCase):
    """Tests for errors module."""
    def test_error(self):
        # Correct case
        for key, _act in ERRORS.items():
            reset_mocks(vars())
            context = Mock()
            # pylint: disable=eval-used
            func = eval(f'errors.Err().{key}')
            res = func(context, 'param')
            sc = getattr(StatusCode, ERRORS[key]['code'])
            if 'msg' in ERRORS[key]:
                msg = sub('%PARAM%', 'param', ERRORS[key]['msg'], 1)
            else:
                msg = 'param'
            context.abort.assert_called_once_with(sc, msg)
        # Error case
        reset_mocks(vars())
        context = Mock()
        res = MOD.Err().unexistent(context)
        self.assertIsNone(res)
        assert not context.abort.called

    @patch(MOD.__name__ + '.getattr')
    def test_report_error(self, mocked_getattr):
        # Mapped errors
        implementations = ['clightning', 'eclair', 'lnd']
        for impl in implementations:
            sett.IMPLEMENTATION = impl
            module = import_module(PROJ_ROOT + f'.light_{impl}')
            # Mapped errors
            err_function = Mock()
            mocked_getattr.return_value = err_function
            err_function.side_effect = Exception()
            for msg, act in module.ERRORS.items():
                reset_mocks(vars())
                error = f'aaa {msg} zzz'
                context = Mock()
                err_self = MOD.Err()
                with self.assertRaises(Exception):
                    err_self.report_error(context, error)
                args = [context, act['params']] if 'params' in act \
                    else [context]
                if act['fun'] == 'node_error':
                    args = [context, error]
                mocked_getattr.assert_called_once_with(err_self, act['fun'])
                err_function.assert_called_once_with(*args)
            # Unmapped error
            reset_mocks(vars())
            context = Mock()
            err_self = MOD.Err()
            err_self.unexpected_error = Mock()
            err_self.unexpected_error.side_effect = Exception()
            with self.assertRaises(Exception):
                err_self.report_error(context, 'unmapped error')
            assert not mocked_getattr.called
            err_self.unexpected_error.assert_called_once_with(
                context, 'unmapped error')
