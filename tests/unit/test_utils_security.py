# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for utils.security module."""

from importlib import import_module
from unittest import TestCase
from unittest.mock import Mock, patch

from nacl.exceptions import CryptoError

from .. import PROJ_ROOT

# pylint: disable=invalid-name

CTX = 'context'
ImplementationSecret = getattr(import_module(PROJ_ROOT + '.db'),
                               'ImplementationSecret')
pb = import_module(PROJ_ROOT + '.boltlight_pb2')
sett = import_module(PROJ_ROOT + '.settings')

MOD = import_module(PROJ_ROOT + '.utils.security')


class UtilsSecurityTests(TestCase):
    """Tests for utils.security module."""
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.Crypter')
    @patch(MOD.__name__ + '.ScryptParams', autospec=True)
    @patch(MOD.__name__ + '.get_token_from_db', autospec=True)
    def test_check_password(self, mocked_db_tok, mocked_params, mocked_crypter,
                            mocked_err):
        pwd = 'password'
        ses = 'session'
        # Correct
        mocked_db_tok.return_value = ['token', 'params']
        mocked_crypter.decrypt.return_value = sett.ACCESS_TOKEN
        res = MOD.check_password(CTX, ses, pwd)
        self.assertIsNone(res)
        mocked_params.assert_called_once_with('')
        mocked_err().wrong_password.assert_not_called()
        # Wrong
        wrong_token = 'wrong_token'
        mocked_crypter.decrypt.return_value = wrong_token
        MOD.check_password(CTX, ses, pwd)
        mocked_err().wrong_password.assert_called_once_with(CTX)

    @patch(MOD.__name__ + '.Crypter')
    @patch(MOD.__name__ + '.ScryptParams', autospec=True)
    @patch(MOD.__name__ + '.get_secret_from_db', autospec=True)
    def test_get_secret(self, mocked_db_sec, mocked_params, mocked_crypter):
        ses = 'session'
        pwd = 'password'
        impl = 'implementation'
        sec_type = 'macaroon'
        # active_only=False (default) with secret case
        res = MOD.get_secret(CTX, ses, pwd, impl, sec_type)
        self.assertEqual(res, mocked_crypter.decrypt.return_value)
        mocked_params.assert_called_once_with('')
        # active_only=False (default) with no secret case
        mocked_db_sec.return_value = None
        res = MOD.get_secret(CTX, ses, pwd, impl, sec_type)
        self.assertEqual(res, None)
        # active_only=True and inactive secret case
        mocked_db_sec.return_value = ImplementationSecret(implementation=impl,
                                                          active=0,
                                                          secret=b'sec')
        res = MOD.get_secret(CTX, ses, pwd, impl, sec_type, active_only=True)
        self.assertEqual(res, None)

    @patch(MOD.__name__ + '.check_password', autospec=True)
    @patch(MOD.__name__ + '.session_scope', autospec=True)
    def test_unlock_node_with_password(self, mocked_ses, mocked_check):
        pwd = 'password'
        req = pb.UnlockNodeRequest(password=pwd)
        unlock_method = Mock()
        res = MOD.unlock_node_with_password(CTX, req, unlock_method)
        self.assertEqual(res, pb.UnlockNodeResponse())
        mocked_ses.assert_called_once_with(CTX)
        mocked_check.assert_called_once_with(
            CTX, mocked_ses.return_value.__enter__.return_value, pwd)
        unlock_method.assert_called_once_with(
            CTX, pwd, session=mocked_ses.return_value.__enter__.return_value)

    @patch(MOD.__name__ + '.Err')
    def test_Crypter(self, mocked_err):
        password = 'boltlightrocks'
        plain_data = b'boltlight is cool'
        params = MOD.ScryptParams(b'salt')
        # Crypt
        derived_key = MOD.Crypter.gen_derived_key(password, params)
        crypt_data = MOD.Crypter.crypt(plain_data, derived_key)
        # Decrypt
        decrypted_data = MOD.Crypter.decrypt(CTX, crypt_data, derived_key)
        self.assertEqual(plain_data, decrypted_data)
        # Error case
        with patch('nacl.secret.SecretBox') as mocked_box:
            mocked_box.return_value.decrypt.side_effect = CryptoError
            decrypted_data = MOD.Crypter.decrypt(CTX, crypt_data, derived_key)
            mocked_err().wrong_password.assert_called_once_with(CTX)
            mocked_err().reset_mock()
        # Wrong version case
        wrong_data = 'random_wrong_stuff'
        MOD.Crypter.decrypt(CTX, wrong_data, derived_key)
        mocked_err().wrong_password.assert_called_once_with(CTX)

    def test_ScryptParams(self):
        salt = b'salt'
        scrypt_params = MOD.ScryptParams(salt)
        serialized = scrypt_params.serialize()
        scrypt_params = MOD.ScryptParams('')
        scrypt_params.deserialize(serialized)
        self.assertEqual(scrypt_params.salt, salt)
