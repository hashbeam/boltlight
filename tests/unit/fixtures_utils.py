# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Fixtures for utils package."""

from importlib import import_module

from .. import PROJ_ROOT

# pylint: disable=invalid-name

pb = import_module(PROJ_ROOT + '.boltlight_pb2')


class FakeMetadatum():  # pylint: disable=too-few-public-methods
    """Fake macaroon metadata."""
    def __init__(self, key, value):
        self.key = key
        self.value = value


CHANNELS = [
    pb.Channel(local_balance_msat=311100,
               remote_balance_msat=66600,
               local_reserve_sat=29,
               remote_reserve_sat=66,
               active=1,
               state=pb.Channel.OPEN),
    pb.Channel(local_balance_msat=666000,
               remote_balance_msat=555000,
               local_reserve_sat=40,
               remote_reserve_sat=33,
               active=0,
               state=pb.Channel.OPEN),
    pb.Channel(local_balance_msat=473000,
               remote_balance_msat=237100,
               local_reserve_sat=77,
               remote_reserve_sat=24,
               active=1,
               state=pb.Channel.OPEN),
    pb.Channel(local_balance_msat=577000,
               remote_balance_msat=2531000,
               local_reserve_sat=30,
               remote_reserve_sat=40,
               active=0,
               state=pb.Channel.PENDING_OPEN),
]

LISTCHANNELRESPONSE = pb.ListChannelsResponse(channels=CHANNELS)

METADATA = (FakeMetadatum(key='macaroon', value='stuff'),
            FakeMetadatum(key='user-agent', value='stuff'))

NODE_ID = "021f7b8bbfbca12b6520683fe39aa80316b729b49db6735a164ad019f81485a684"
HOST = "snoopy"
PORT = 9735
HOST_ADDRESS = f'{HOST}:{PORT}'
NODE_URI = f'{NODE_ID}@{HOST_ADDRESS}'
