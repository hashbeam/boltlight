# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for light_lnd module."""

from codecs import encode
from concurrent.futures import TimeoutError as TimeoutFutError
from importlib import import_module
from unittest import TestCase
from unittest.mock import Mock, call, mock_open, patch

from grpc import FutureTimeoutError, RpcError
from lnd_proto import lightning_pb2 as ln

from .. import PROJ_ROOT, reset_mocks
from . import fixtures_lnd as fix

# pylint: disable=invalid-name, protected-access, too-many-arguments
# pylint: disable=too-many-locals, too-many-public-methods, too-many-statements
# pylint: disable=too-many-lines

CTX = 'context'
Enf = getattr(import_module(PROJ_ROOT + '.utils.bitcoin'), 'Enforcer')
LND_LN_TX = getattr(import_module(PROJ_ROOT + '.light_lnd'), 'LND_LN_TX')
pb = import_module(PROJ_ROOT + '.boltlight_pb2')
sett = import_module(PROJ_ROOT + '.settings')

MOD = import_module(PROJ_ROOT + '.light_lnd')


class LightLndTests(TestCase):
    """Tests for light_lnd module."""

    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_get_node_version(self, mocked_connect, mocked_get_time):
        stub = mocked_connect.return_value.__enter__.return_value
        # node answers case
        stub.GetInfo.return_value = fix.GETINFO_MAINNET
        res = MOD.get_node_version()
        self.assertEqual(res, fix.GETINFO_MAINNET.version)
        # node is not responsive case
        stub.GetInfo.side_effect = RuntimeError
        res = MOD.get_node_version()
        self.assertEqual(res, '')
        # method returns error case
        stub.GetInfo.side_effect = fix.CalledRpcError()
        res = MOD.get_node_version()
        self.assertEqual(res, '')

    @patch(MOD.__name__ + '.ssl_channel_credentials')
    @patch(MOD.__name__ + '.get_path', autospec=True)
    @patch(MOD.__name__ + '.set_defaults')
    def test_get_settings(self, mocked_set_def, mocked_get_path,
                          mocked_ssl_chan):
        # Correct case: with macaroons
        lnd_host = 'lnd'
        lnd_port = '10009'
        lnd_tls_cert_dir = '/path'
        lnd_tls_cert = 'tls.cert'
        config = Mock()
        config.get.side_effect = \
            [lnd_host, lnd_port, lnd_tls_cert_dir, lnd_tls_cert]
        mocked_ssl_chan.return_value = 'cert_creds'
        mocked_get_path.return_value = lnd_tls_cert_dir
        mopen = mock_open(read_data='cert')
        with patch(MOD.__name__ + '.open', mopen):
            MOD.get_settings(config, 'lnd')
        lnd_values = ['LND_HOST', 'LND_PORT', 'LND_CERT']
        mocked_set_def.assert_called_once_with(config, lnd_values)
        mopen.assert_called_with('/path/tls.cert', 'rb')
        mopen.return_value.read.assert_called_once_with()
        mocked_ssl_chan.assert_called_with('cert')
        self.assertEqual(sett.LND_ADDR, f'{lnd_host}:{lnd_port}')
        self.assertEqual(sett.LND_CREDS_FULL, 'cert_creds')
        self.assertEqual(sett.IMPL_SEC_TYPE, 'macaroon')

    @patch(MOD.__name__ + '.composite_channel_credentials')
    @patch(MOD.__name__ + '.metadata_call_credentials')
    @patch(MOD.__name__ + '._metadata_callback')
    def test_update_settings(self, mocked_callback, mocked_meta_call,
                             mocked_comp_chan):
        # Correct case: with macaroons
        sett.LND_CREDS_SSL = 'cert_creds'
        mocked_meta_call.return_value = 'auth_creds'
        mocked_comp_chan.return_value = 'combined_creds'
        MOD.update_settings('mac')
        mocked_meta_call.assert_called_with(mocked_callback)
        mocked_comp_chan.assert_called_with('cert_creds', 'auth_creds')
        self.assertEqual(sett.LND_CREDS_FULL, 'combined_creds')
        # Correct case: without macaroons
        reset_mocks(vars())
        sett.LND_CREDS_FULL = 'cert_creds'
        MOD.update_settings(None)
        assert not mocked_meta_call.called
        assert not mocked_comp_chan.called
        self.assertEqual(sett.LND_CREDS_FULL, 'cert_creds')

    def test_metadata_callback(self):
        sett.LND_MAC = b'macaroon_bytes'
        mac = encode(sett.LND_MAC, 'hex')
        mocked_callback = Mock()
        res = MOD._metadata_callback(CTX, mocked_callback)
        self.assertIsNone(res)
        mocked_callback.assert_called_once_with([('macaroon', mac)], None)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    def test_handle_rpc_errors(self, mocked_handle_err):
        # Correct case
        func = Mock()
        func.return_value = '777'
        wrapped = MOD._handle_rpc_errors(func)
        res = wrapped(3)
        self.assertEqual(res, '777')
        self.assertEqual(func.call_count, 1)
        # Error case
        reset_mocks(vars())
        error = RpcError()
        func.side_effect = error
        wrapped = MOD._handle_rpc_errors(func)
        res = wrapped(3, 'context')
        self.assertEqual(res, mocked_handle_err.return_value)
        self.assertEqual(func.call_count, 1)
        mocked_handle_err.assert_called_once_with('context', error)

    @patch(MOD.__name__ + '.wu_lnrpc.WalletUnlockerStub', autospec=True)
    @patch(MOD.__name__ + '.lnrpc.LightningStub', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '.channel_ready_future', autospec=True)
    @patch(MOD.__name__ + '.secure_channel', autospec=True)
    def test_connect(self, mocked_secure_chan, mocked_future, mocked_get_time,
                     mocked_err, mocked_ln_stub, mocked_wu_stub):
        sett.LND_ADDR = 'lnd:10009'
        sett.LND_CREDS_FULL = 'creds'
        sett.LND_CREDS_SSL = 'cert'
        # correct case
        with MOD._connect(CTX) as stub:
            self.assertEqual(stub, mocked_ln_stub.return_value)
        mocked_secure_chan.assert_called_once_with('lnd:10009', 'creds')
        mocked_get_time.assert_called_once_with(CTX)
        mocked_ln_stub.assert_called_once_with(mocked_secure_chan.return_value)
        mocked_secure_chan.return_value.close.assert_called_once_with()
        # with different stub_class and force_no_macaroon=True case
        reset_mocks(vars())
        with MOD._connect(CTX,
                          stub_class=MOD.wu_lnrpc.WalletUnlockerStub,
                          force_no_macaroon=True) as stub:
            self.assertEqual(stub, mocked_wu_stub.return_value)
        mocked_secure_chan.assert_called_once_with('lnd:10009', 'cert')
        assert not mocked_ln_stub.called
        mocked_secure_chan.return_value.close.assert_called_once_with()
        # error case
        reset_mocks(vars())
        mocked_future.return_value.result.side_effect = FutureTimeoutError()
        mocked_err().node_error.side_effect = Exception()
        with self.assertRaises(Exception):
            with MOD._connect(CTX) as stub:
                self.assertEqual(stub, 'stub')

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.LOGGER', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.get_secret', autospec=True)
    @patch(MOD.__name__ + '.session_scope', autospec=True)
    @patch(MOD.__name__ + '.ExitStack', autospec=True)
    def test_unlock_node(self, mocked_stack, mocked_ses, mocked_get_sec,
                         mocked_err, mocked_connect, mocked_get_time,
                         mocked_log, mocked_handle_err):
        pwd = 'password'
        stub = mocked_connect.return_value.__enter__.return_value
        lnd_pwd = b'lnd_password'
        mocked_get_sec.return_value = lnd_pwd
        mocked_err().node_error.side_effect = Exception()
        mocked_handle_err.side_effect = Exception()
        # with no session, correct password
        MOD.unlock_node(CTX, pwd)
        mocked_ses.assert_called_once_with(CTX)
        mocked_connect.assert_called_once_with(
            CTX,
            stub_class=MOD.wu_lnrpc.WalletUnlockerStub,
            force_no_macaroon=True)
        mocked_get_time.assert_called_once_with(CTX)
        # with no session, incorrect password
        reset_mocks(vars())
        err = 'invalid passphrase for master public key'
        stub.UnlockWallet.side_effect = RpcError(err)
        with self.assertRaises(Exception):
            MOD.unlock_node(CTX, pwd)
        mocked_handle_err.assert_called_once_with(CTX, err)
        # with no session, already unlocked
        reset_mocks(vars())
        stub.UnlockWallet.side_effect = RpcError(
            'wallet already unlocked, WalletUnlocker service is no longer '
            'available')
        MOD.unlock_node(CTX, pwd)
        assert not mocked_err().node_error.called
        assert mocked_log.info.called
        # with no session, other errors
        reset_mocks(vars())
        err = 'error'
        stub.UnlockWallet.side_effect = RpcError(err)
        with self.assertRaises(Exception):
            MOD.unlock_node(CTX, pwd)
        assert not mocked_err().node_error.called
        assert not mocked_log.info.called
        mocked_handle_err.assert_called_once_with(CTX, err)
        # with session, no password stored
        reset_mocks(vars())
        stub.UnlockWallet.side_effect = None
        ses = 'session'
        mocked_get_sec.return_value = None
        with self.assertRaises(Exception):
            MOD.unlock_node(CTX, pwd, session=ses)
        mocked_stack.assert_called_once_with()
        assert not mocked_ses.called
        assert mocked_err().node_error.called

    @patch(MOD.__name__ + '.unlock_node_with_password', autospec=True)
    def test_UnlockNode(self, mocked_unlock):
        req = pb.UnlockNodeRequest(password='password')
        res = MOD.UnlockNode(req, CTX)
        self.assertEqual(res, mocked_unlock.return_value)
        mocked_unlock.assert_called_once_with(CTX, req, MOD.unlock_node)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_GetNodeInfo(self, mocked_connect, mocked_get_time, mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        # Testnet case
        lnd_res = fix.GETINFO_TESTNET
        stub.GetInfo.return_value = lnd_res
        res = MOD.GetNodeInfo('request', CTX)
        stub.GetInfo.assert_called_once_with(ln.GetInfoRequest(), timeout=time)
        assert not mocked_handle.called
        self.assertEqual(res.identity_pubkey, 'asd')
        self.assertEqual(res.network, pb.Network.TESTNET)
        self.assertEqual(res.color, '#DCDCDC')
        # Mainnet case
        reset_mocks(vars())
        stub.GetInfo.return_value = fix.GETINFO_MAINNET
        res = MOD.GetNodeInfo('request', CTX)
        self.assertEqual(res.network, pb.Network.MAINNET)
        # Regtest case
        reset_mocks(vars())
        stub.GetInfo.return_value = fix.GETINFO_REGTEST
        res = MOD.GetNodeInfo('request', CTX)
        self.assertEqual(res.network, pb.Network.REGTEST)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_NewAddress(self, mocked_connect, mocked_get_time, mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        # P2WPKH case
        request = pb.NewAddressRequest()
        lnd_res = ln.NewAddressResponse(address='addr')
        stub.NewAddress.return_value = lnd_res
        res = MOD.NewAddress(request, CTX)
        stub.NewAddress.assert_called_once_with(ln.NewAddressRequest(),
                                                timeout=time)
        assert not mocked_handle.called
        self.assertEqual(res.address, 'addr')
        # NP2KWH case
        reset_mocks(vars())
        request = pb.NewAddressRequest(addr_type=pb.Address.NP2WPKH)
        lnd_res = ln.NewAddressResponse(address='addr')
        stub.NewAddress.return_value = lnd_res
        res = MOD.NewAddress(request, CTX)
        stub.NewAddress.assert_called_with(ln.NewAddressRequest(type=1),
                                           timeout=time)
        assert not mocked_handle.called
        self.assertEqual(res.address, 'addr')

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_BalanceOnChain(self, mocked_connect, mocked_get_time,
                            mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = time
        # Correct case
        stub.WalletBalance.return_value = fix.WALLETBALANCERESPONSE
        res = MOD.BalanceOnChain('request', CTX)
        stub.WalletBalance.assert_called_once_with(ln.WalletBalanceRequest(),
                                                   timeout=time)
        assert not mocked_handle.called
        self.assertEqual(res.total_sat,
                         fix.WALLETBALANCERESPONSE.total_balance)
        self.assertEqual(res.confirmed_sat,
                         fix.WALLETBALANCERESPONSE.confirmed_balance)

    @patch(MOD.__name__ + '.get_channel_balances', autospec=True)
    @patch(MOD.__name__ + '.ListChannels', autospec=True)
    def test_BalanceOffChain(self, mocked_ListChannels, mocked_get_chan_bal):
        mocked_get_chan_bal.return_value = pb.BalanceOffChainResponse()
        res = MOD.BalanceOffChain('request', CTX)
        assert mocked_ListChannels.called
        self.assertEqual(res, pb.BalanceOffChainResponse())

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '._add_channel', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_ListChannels(self, mocked_connect, mocked_get_time, mocked_add,
                          mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        # Correct case: request.active_only = False
        request = pb.ListChannelsRequest()
        lnd_res_act = ln.ListChannelsResponse()
        lnd_res_act.channels.add()
        stub.ListChannels.return_value = lnd_res_act
        lnd_res_pen = ln.PendingChannelsResponse()
        lnd_res_pen.pending_open_channels.add()
        lnd_res_pen.pending_closing_channels.add()
        lnd_res_pen.pending_force_closing_channels.add()
        lnd_res_pen.waiting_close_channels.add()
        stub.PendingChannels.return_value = lnd_res_pen
        res = MOD.ListChannels(request, CTX)
        calls = [
            call(pb.ListChannelsResponse(),
                 lnd_res_act.channels[0],
                 pb.Channel.OPEN,
                 active_only=False,
                 open_chan=True),
            call(pb.ListChannelsResponse(),
                 lnd_res_pen.pending_open_channels[0],
                 pb.Channel.PENDING_OPEN),
            call(pb.ListChannelsResponse(),
                 lnd_res_pen.pending_closing_channels[0],
                 pb.Channel.PENDING_MUTUAL_CLOSE),
            call(pb.ListChannelsResponse(),
                 lnd_res_pen.pending_force_closing_channels[0],
                 pb.Channel.PENDING_FORCE_CLOSE),
            call(pb.ListChannelsResponse(),
                 lnd_res_pen.waiting_close_channels[0], pb.Channel.UNKNOWN)
        ]
        mocked_add.assert_has_calls(calls)
        stub.ListChannels.assert_called_once_with(ln.ListChannelsRequest(),
                                                  timeout=time)
        lnd_req = ln.PendingChannelsRequest()
        stub.PendingChannels.assert_called_once_with(lnd_req, timeout=time)
        assert not mocked_handle.called
        self.assertEqual(res, pb.ListChannelsResponse())
        # Correct case: request.active_only = True
        reset_mocks(vars())
        request = pb.ListChannelsRequest(active_only=True)
        lnd_res = ln.ListChannelsResponse()
        lnd_res.channels.add(active=True)
        stub.ListChannels.return_value = lnd_res
        res = MOD.ListChannels(request, CTX)
        stub.ListChannels.assert_called_once_with(ln.ListChannelsRequest(),
                                                  timeout=time)
        mocked_add.assert_called_once_with(pb.ListChannelsResponse(),
                                           lnd_res.channels[0],
                                           pb.Channel.OPEN,
                                           active_only=True,
                                           open_chan=True)
        assert not stub.PendingChannels.called
        assert not mocked_handle.called
        self.assertEqual(res, pb.ListChannelsResponse())

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '._parse_invoices', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_ListInvoices(self, mocked_connect, mocked_get_time, mocked_parse,
                          mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        # Correct case: with max_items, search_order 1
        max_invoices = 50
        request = pb.ListInvoicesRequest(max_items=max_invoices,
                                         search_order=1)
        lnd_res = fix.get_invoices_response(request)
        stub.ListInvoices.return_value = lnd_res
        mocked_parse.side_effect = [False, True]
        res = MOD.ListInvoices(request, CTX)
        self.assertIsNotNone(res)
        self.assertEqual(request.max_items, max_invoices)
        req = ln.ListInvoiceRequest(num_max_invoices=max_invoices *
                                    sett.INVOICES_TIMES,
                                    reversed=True,
                                    index_offset=1)
        stub.ListInvoices.assert_called_with(req, timeout=time)
        self.assertEqual(stub.ListInvoices.call_count, 2)
        assert not mocked_handle.called
        # Correct case: without max_items, search_order 0
        reset_mocks(vars())
        request = pb.ListInvoicesRequest()
        mocked_parse.side_effect = [False, True]
        MOD.ListInvoices(request, CTX)
        self.assertEqual(request.max_items, sett.MAX_INVOICES)
        req = ln.ListInvoiceRequest(num_max_invoices=sett.MAX_INVOICES *
                                    sett.INVOICES_TIMES,
                                    index_offset=len(fix.INVOICES))
        stub.ListInvoices.assert_called_with(req, timeout=time)
        self.assertEqual(stub.ListInvoices.call_count, 2)
        assert not mocked_handle.called
        # Empty response case
        reset_mocks(vars())
        request = pb.ListInvoicesRequest()
        stub.ListInvoices.return_value = ln.ListInvoiceResponse()
        MOD.ListInvoices(request, CTX)
        assert not mocked_parse.called
        assert not mocked_handle.called

    @patch(MOD.__name__ + '._add_payment', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_ListPayments(self, mocked_connect, mocked_get_time, mocked_add):
        stub = mocked_connect.return_value.__enter__.return_value
        mocked_get_time.return_value = 10
        # Correct case
        request = pb.ListPaymentsRequest()
        lnd_res = fix.get_listpayments_response()
        stub.ListPayments.return_value = lnd_res
        res = MOD.ListPayments(request, CTX)
        self.assertIsNotNone(res)
        response = pb.ListPaymentsResponse()
        calls = []
        for lnd_payment in lnd_res.payments:
            calls.append(call(response, lnd_payment))
        mocked_add.assert_has_calls(calls)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_ListPeers(self, mocked_connect, mocked_get_time, mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        # Filled case
        lnd_res = ln.ListPeersResponse()
        lnd_res.peers.add(pub_key='pubkey', address='address')
        stub.ListPeers.return_value = lnd_res
        lnd_res = ln.NodeInfo(node=ln.LightningNode(alias='alias'))
        stub.GetNodeInfo.return_value = lnd_res
        res = MOD.ListPeers('request', CTX)
        stub.ListPeers.assert_called_once_with(ln.ListPeersRequest(),
                                               timeout=time)
        assert not mocked_handle.called
        self.assertEqual(res.peers[0].pubkey, 'pubkey')
        self.assertEqual(res.peers[0].address, 'address')
        self.assertEqual(res.peers[0].alias, 'alias')
        assert not mocked_handle.called
        # Empty case
        reset_mocks(vars())
        stub.ListPeers.return_value = pb.ListPeersResponse()
        res = MOD.ListPeers('request', CTX)
        stub.ListPeers.assert_called_once_with(ln.ListPeersRequest(),
                                               timeout=time)
        assert not mocked_handle.called
        self.assertEqual(res, pb.ListPeersResponse())

    @patch(MOD.__name__ + '._add_transaction', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_ListTransactions(self, mocked_connect, mocked_get_time,
                              mocked_add):
        stub = mocked_connect.return_value.__enter__.return_value
        mocked_get_time.return_value = 10
        # Correct case
        request = pb.ListTransactionsRequest()
        lnd_res = fix.get_transactions_response()
        stub.GetTransactions.return_value = lnd_res
        res = MOD.ListTransactions(request, CTX)
        self.assertIsNotNone(res)
        response = pb.ListTransactionsResponse()
        calls = []
        for lnd_transaction in lnd_res.transactions:
            calls.append(call(response, lnd_transaction))
        mocked_add.assert_has_calls(calls)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.Enf.check_value')
    def test_CreateInvoice(self, mocked_check_val, mocked_err, mocked_connect,
                           mocked_get_time, mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        amt = 7
        desc = "description"
        fa = "fallback_address"
        exp = 3600
        # Correct case: filled
        request = pb.CreateInvoiceRequest(amount_msat=amt,
                                          description=desc,
                                          expiry=exp,
                                          min_final_cltv_expiry=amt,
                                          fallback_addr=fa)
        mocked_check_val.return_value = True
        stub.AddInvoice.return_value = fix.ADDINVOICERESPONSE
        lnd_res = ln.Invoice(creation_date=1534971310, expiry=exp)
        stub.LookupInvoice.return_value = lnd_res
        res = MOD.CreateInvoice(request, CTX)
        req = ln.Invoice(memo=desc,
                         expiry=exp,
                         fallback_addr=fa,
                         value_msat=amt,
                         cltv_expiry=amt)
        stub.AddInvoice.assert_called_once_with(req, timeout=time)
        lnd_req = ln.PaymentHash(r_hash=stub.AddInvoice.return_value.r_hash)
        stub.LookupInvoice.assert_called_once_with(lnd_req, timeout=time)
        assert not mocked_handle.called
        self.assertEqual(
            res.payment_hash,
            MOD.hexlify(stub.AddInvoice.return_value.r_hash).decode())
        self.assertEqual(res.expires_at, 1534974910)
        # Correct case: empty request
        reset_mocks(vars())
        request = pb.CreateInvoiceRequest()
        lnd_res = ln.AddInvoiceResponse()
        stub.AddInvoice.return_value = lnd_res
        res = MOD.CreateInvoice(request, CTX)
        stub.AddInvoice.assert_called_once_with(
            ln.Invoice(expiry=sett.EXPIRY_TIME), timeout=time)
        assert not stub.LookupInvoice.called
        assert not mocked_handle.called
        self.assertEqual(res, pb.CreateInvoiceResponse())
        # min_final_cltv_expiry out of range case
        reset_mocks(vars())
        request = pb.CreateInvoiceRequest(amount_msat=amt,
                                          min_final_cltv_expiry=amt)
        mocked_check_val.side_effect = Exception()
        with self.assertRaises(Exception):
            res = MOD.CreateInvoice(request, CTX)
        assert not mocked_connect.called
        mocked_check_val.side_effect = None

    @patch(MOD.__name__ + '._get_invoice_state', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_CheckInvoice(self, mocked_check_par, mocked_connect,
                          mocked_get_time, mocked_inv_st):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        phash = encode(b'apaymenthash', 'hex')
        # Correct case: paid invoice
        mocked_inv_st.return_value = pb.Invoice.PAID
        request = pb.CheckInvoiceRequest(payment_hash=phash)
        lnd_res = ln.Invoice(state=ln.Invoice.SETTLED)
        stub.LookupInvoice.return_value = lnd_res
        res = MOD.CheckInvoice(request, CTX)
        lnd_req = ln.PaymentHash(r_hash=MOD.unhexlify(phash))
        stub.LookupInvoice.assert_called_once_with(lnd_req, timeout=time)
        self.assertEqual(res.state, mocked_inv_st.return_value)
        # Missing parameter case
        reset_mocks(vars())
        request = pb.CheckInvoiceRequest()
        mocked_check_par.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.CheckInvoice(request, CTX)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.Enf.check_value')
    @patch(MOD.__name__ + '.has_amount_encoded', autospec=True)
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_PayInvoice(self, mocked_check_par, mocked_has_amt,
                        mocked_check_val, mocked_err, mocked_connect,
                        mocked_get_time, mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        # Amount in invoice but not in request case
        request = pb.PayInvoiceRequest(payment_request='something',
                                       cltv_expiry_delta=7)
        mocked_has_amt.return_value = True
        mocked_check_val.return_value = True
        stub.SendPaymentSync.return_value = fix.SENDRESPONSE
        stub.SendPaymentSync.return_value.payment_error = ''
        res = MOD.PayInvoice(request, CTX)
        assert not mocked_err().unsettable.called
        lnd_req = ln.SendRequest(payment_request='something',
                                 final_cltv_delta=7)
        stub.SendPaymentSync.assert_called_once_with(lnd_req, timeout=time)
        assert not mocked_handle.called
        self.assertEqual(res.payment_preimage,
                         '615f7061796d656e745f707265696d616765')
        # Amount in invoice and in request case
        reset_mocks(vars())
        request = pb.PayInvoiceRequest(payment_request='abc', amount_msat=7)
        mocked_has_amt.return_value = True
        mocked_err().unsettable.side_effect = Exception()
        with self.assertRaises(Exception):
            res = MOD.PayInvoice(request, CTX)
        mocked_err().unsettable.assert_called_once_with(CTX, 'amount_msat')
        assert not stub.SendPaymentSync.called
        assert not mocked_handle.called
        # Amount in request and not in invoice case
        reset_mocks(vars())
        request = pb.PayInvoiceRequest(payment_request='abc', amount_msat=7)
        mocked_has_amt.return_value = False
        MOD.PayInvoice(request, CTX)
        mocked_check_val.assert_called_once_with(CTX, request.amount_msat,
                                                 LND_LN_TX)
        # Amount neither in request or invoice case
        reset_mocks(vars())
        mocked_check_par.side_effect = [None, Exception()]
        request = pb.PayInvoiceRequest(payment_request='random')
        mocked_has_amt.return_value = False
        with self.assertRaises(Exception):
            res = MOD.PayInvoice(request, CTX)
        self.assertEqual(mocked_check_par.call_count, 2)
        assert not mocked_connect.called
        assert not mocked_handle.called
        # cltv_expiry_delta out of range case
        reset_mocks(vars())
        mocked_check_par.side_effect = None
        request = pb.PayInvoiceRequest(payment_request='abc',
                                       cltv_expiry_delta=7)
        mocked_check_val.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.PayInvoice(request, CTX)
        assert not mocked_connect.called
        mocked_check_val.side_effect = None
        # Error from lnd_res.payment_error case
        reset_mocks(vars())
        request = pb.PayInvoiceRequest(payment_request='something')
        stub.SendPaymentSync.return_value = fix.SENDRESPONSE_ERR
        res = MOD.PayInvoice(request, CTX)
        lnd_req = ln.SendRequest(payment_request='something')
        stub.SendPaymentSync.assert_called_once_with(lnd_req, timeout=time)
        assert not mocked_err().unsettable.called
        lnd_req = ln.SendRequest(payment_request='something')
        stub.SendPaymentSync.assert_called_once_with(lnd_req, timeout=time)
        mocked_handle.assert_called_once_with(
            CTX, fix.SENDRESPONSE_ERR.payment_error)
        # Empty response case
        reset_mocks(vars())
        request = pb.PayInvoiceRequest(payment_request='something')
        stub.SendPaymentSync.return_value.payment_preimage = b''
        stub.SendPaymentSync.return_value.payment_error = ''
        res = MOD.PayInvoice(request, CTX)
        lnd_req = ln.SendRequest(payment_request='something')
        stub.SendPaymentSync.assert_called_once_with(lnd_req, timeout=time)
        self.assertEqual(res, pb.PayInvoiceResponse())

    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.Enf.check_value')
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_PayOnChain(self, mocked_check_par, mocked_connect,
                        mocked_get_time, mocked_check_val, mocked_err):
        stub = mocked_connect.return_value.__enter__.return_value
        mocked_get_time.return_value = 10
        amt = 7
        # Correct case
        request = pb.PayOnChainRequest(address=fix.ADDRESS,
                                       amount_sat=amt,
                                       fee_sat_byte=1)
        stub.SendCoins.return_value = fix.SENDCOINSRESPONSE
        MOD.PayOnChain(request, CTX)
        # Missing parameter case
        reset_mocks(vars())
        request = pb.PayOnChainRequest()
        mocked_check_par.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.PayOnChain(request, CTX)
        # Incorrect fee_sat_byte case
        reset_mocks(vars())
        mocked_check_par.side_effect = None
        request = pb.PayOnChainRequest(address=fix.ADDRESS,
                                       amount_sat=amt,
                                       fee_sat_byte=int(2**32 - 1))
        mocked_check_val.side_effect = Exception()
        stub.SendCoins.return_value = ln.SendCoinsResponse()
        with self.assertRaises(Exception):
            MOD.PayOnChain(request, CTX)
        mocked_check_val.side_effect = None

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '._add_route_hint', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_DecodeInvoice(self, mocked_check_par, mocked_err, mocked_connect,
                           mocked_get_time, mocked_add, mocked_handle):
        stub = mocked_connect.return_value.__enter__.return_value
        mocked_get_time.return_value = 10
        mocked_err().unimplemented_parameter.side_effect = Exception()
        # Filled
        request = pb.DecodeInvoiceRequest(payment_request='pay_req')
        stub.DecodePayReq.return_value = fix.PAYREQ
        res = MOD.DecodeInvoice(request, CTX)
        response = pb.DecodeInvoiceResponse(
            amount_msat=fix.PAYREQ.num_satoshis * 1000)
        calls = [
            call(response, fix.PAYREQ.route_hints[0]),
            call(response, fix.PAYREQ.route_hints[1])
        ]
        mocked_add.assert_has_calls(calls)
        # Unimplemented parameter description case
        reset_mocks(vars())
        request = pb.DecodeInvoiceRequest(payment_request='pay_req',
                                          description='d')
        with self.assertRaises(Exception):
            MOD.DecodeInvoice(request, CTX)
        # Missing parameter case
        reset_mocks(vars())
        request = pb.DecodeInvoiceRequest(payment_request='pay_req')
        mocked_check_par.side_effect = Exception()
        request = pb.DecodeInvoiceRequest()
        with self.assertRaises(Exception):
            MOD.DecodeInvoice(request, CTX)
        assert not stub.DecodePayReq.called
        assert not mocked_add.called
        assert not mocked_handle.called
        mocked_check_par.side_effect = None

    @patch(MOD.__name__ + '._txid_bytes_to_str', autospec=True)
    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.convert', autospec=True)
    @patch(MOD.__name__ + '.Enf.check_value')
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_OpenChannel(self, mocked_check_par, mocked_err, mocked_connect,
                         mocked_get_time, mocked_check_val, mocked_conv,
                         mocked_handle, mocked_txid_bs):
        stub = mocked_connect.return_value.__enter__.return_value
        time = 10
        mocked_get_time.return_value = 10
        txid = 'txid'
        mocked_txid_bs.return_value = txid
        mocked_err().value_too_high.side_effect = Exception()
        # Filled
        request = pb.OpenChannelRequest(funding_sat=7,
                                        node_uri=fix.NODE_URI,
                                        push_msat=2000,
                                        private=True)
        stub.OpenChannelSync.return_value = fix.CHANNELPOINT
        mocked_conv.return_value = 2
        MOD.OpenChannel(request, CTX)
        mocked_connect.assert_called_once_with(CTX)
        peer_address = ln.LightningAddress(pubkey=fix.NODE_ID,
                                           host=f'{fix.HOST}:{fix.PORT}')
        lnd_req = ln.ConnectPeerRequest(addr=peer_address, perm=True)
        stub.ConnectPeer.assert_called_once_with(lnd_req, timeout=time)
        lnd_req = ln.OpenChannelRequest(
            node_pubkey=MOD.unhexlify(fix.NODE_ID),
            local_funding_amount=request.funding_sat,
            push_sat=mocked_conv.return_value,
            private=True)
        stub.OpenChannelSync.assert_called_once_with(lnd_req, timeout=time)
        # error: push amount too high
        reset_mocks(vars())
        request.push_msat = 1000 * request.funding_sat
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        mocked_err().value_too_high.assert_called_once_with(
            CTX, request.push_msat)
        assert not stub.OpenChannelSync.called
        # already connected peer case
        reset_mocks(vars())
        request.push_msat = 77
        stub.ConnectPeer.side_effect = fix.CalledRpcError()
        mocked_err().connect_failed.side_effect = Exception
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        mocked_connect.assert_called_once_with(CTX)
        peer_address = ln.LightningAddress(pubkey=fix.NODE_ID,
                                           host=f'{fix.HOST}:{fix.PORT}')
        lnd_req = ln.ConnectPeerRequest(addr=peer_address, perm=True)
        stub.ConnectPeer.assert_called_once_with(lnd_req, timeout=time)
        assert not stub.OpenChannelSync.called
        # Filled with peer already connected
        reset_mocks(vars())
        stub.ConnectPeer.side_effect = fix.ConnectRpcError()
        MOD.OpenChannel(request, CTX)
        mocked_connect.assert_called_once_with(CTX)
        peer_address = ln.LightningAddress(pubkey=fix.NODE_ID,
                                           host=f'{fix.HOST}:{fix.PORT}')
        lnd_req = ln.ConnectPeerRequest(addr=peer_address, perm=True)
        stub.ConnectPeer.assert_called_once_with(lnd_req, timeout=time)
        lnd_req = ln.OpenChannelRequest(
            node_pubkey=MOD.unhexlify(fix.NODE_ID),
            local_funding_amount=request.funding_sat,
            push_sat=mocked_conv.return_value,
            private=True)
        stub.OpenChannelSync.assert_called_once_with(lnd_req, timeout=time)
        # invalid node_uri case
        reset_mocks(vars())
        request.node_uri = 'wrong'
        mocked_err().invalid.side_effect = Exception
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        mocked_err().invalid.assert_called_once_with(CTX, 'node_uri')
        # Missing parameter case
        reset_mocks(vars())
        request = pb.OpenChannelRequest(funding_sat=request.funding_sat)
        mocked_check_par.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        assert not stub.ConnectPeer.called
        assert not stub.OpenChannelSync.called
        assert not mocked_conv.called
        assert not mocked_handle.called

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.get_thread_timeout', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '.ThreadPoolExecutor', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_CloseChannel(self, mocked_check_par, mocked_err, mocked_connect,
                          mocked_thread, mocked_get_time, mocked_thread_time,
                          mocked_handle):
        mocked_err().invalid.side_effect = Exception()
        mocked_get_time.return_value = 30
        mocked_thread_time.return_value = 2
        txid = 'closed'
        # Correct case
        stub = mocked_connect.return_value.__enter__.return_value
        stub.GetChanInfo.return_value = ln.ChannelEdge(chan_point='1rtfm:0')
        future = Mock()
        executor = Mock()
        future.result.return_value = txid.encode()
        executor.submit.return_value = future
        mocked_thread.return_value.__enter__.return_value = executor
        request = pb.CloseChannelRequest(channel_id='777')
        ctx = Mock()
        ctx.time_remaining.return_value = 300
        res = MOD.CloseChannel(request, ctx)
        self.assertEqual(res.closing_txid, txid)
        mocked_check_par.assert_called_once_with(ctx, request, 'channel_id')
        # Invalid channel_id case
        reset_mocks(vars())
        bad_request = pb.CloseChannelRequest(channel_id='aa7')
        with self.assertRaises(Exception):
            MOD.CloseChannel(bad_request, ctx)
        mocked_err().invalid.assert_called_once_with(ctx, 'channel_id')
        # Result times out
        reset_mocks(vars())
        future.result.side_effect = TimeoutFutError()
        res = MOD.CloseChannel(request, ctx)
        executor.shutdown.assert_called_once_with(wait=False)
        self.assertEqual(res, pb.CloseChannelResponse())
        # Result throws RuntimeError
        # (could be triggered by _connect in _close_channel)
        reset_mocks(vars())
        err = 'err'
        future.result.side_effect = RuntimeError(err)
        MOD.CloseChannel(request, ctx)
        mocked_handle.assert_called_once_with(ctx, err)
        # Result throws RpcError (could be triggered _close_channel)
        reset_mocks(vars())
        error = RpcError(err)
        future.result.side_effect = error
        MOD.CloseChannel(request, ctx)
        mocked_handle.assert_called_once_with(ctx, error)

    @patch(MOD.__name__ + '._txid_bytes_to_str', autospec=True)
    @patch(MOD.__name__ + '.LOGGER', autospec=True)
    @patch(MOD.__name__ + '._connect', autospec=True)
    def test_close_channel(self, mocked_connect, mocked_log, mocked_txid_bs):
        ptxid = 'ptxid'
        mocked_txid_bs.return_value = ptxid
        stub = mocked_connect.return_value.__enter__.return_value
        pending_update = ln.PendingUpdate(txid=ptxid.encode())
        channel_close_update = ln.ChannelCloseUpdate(closing_txid=b'ctxid')
        stub.CloseChannel.return_value = [
            ln.CloseStatusUpdate(close_pending=pending_update),
            ln.CloseStatusUpdate(chan_close=channel_close_update)
        ]
        chan_point = ln.ChannelPoint(funding_txid_str='txid', output_index=0)
        lnd_req = ln.CloseChannelRequest(channel_point=chan_point)
        res = MOD._close_channel(lnd_req, 15)
        self.assertEqual(mocked_log.debug.call_count, 1)
        self.assertEqual(res, ptxid)
        # stub throws RpcError
        reset_mocks(vars())
        stub.CloseChannel.side_effect = fix.CalledRpcError()
        with self.assertRaises(fix.CalledRpcError):
            MOD._close_channel(lnd_req, 15)
        assert mocked_log.debug.called
        # _connect throws RuntimeError
        reset_mocks(vars())
        mocked_connect.side_effect = RuntimeError()
        with self.assertRaises(RuntimeError):
            MOD._close_channel(lnd_req, 15)
        assert not mocked_log.called

    def test_add_channel(self):
        # Active: empty
        response = pb.ListChannelsResponse()
        lnd_chan = ln.Channel()
        MOD._add_channel(response, lnd_chan, pb.Channel.OPEN, open_chan=True)
        self.assertEqual(response, pb.ListChannelsResponse())
        # Active: filled, initiator=True, remote balance below dust limit
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        MOD._add_channel(response,
                         fix.OPEN_CHAN_INITIATOR_BELOW_DUST,
                         pb.Channel.OPEN,
                         open_chan=True)
        self.assertEqual(response.channels[0].channel_id,
                         str(fix.OPEN_CHAN_INITIATOR_BELOW_DUST.chan_id))
        loc_bal = fix.OPEN_CHAN_INITIATOR_BELOW_DUST.local_balance + \
            fix.OPEN_CHAN_INITIATOR_BELOW_DUST.commit_fee - \
            fix.OPEN_CHAN_INITIATOR_BELOW_DUST.remote_balance
        rem_bal = fix.OPEN_CHAN_INITIATOR_BELOW_DUST.remote_balance
        self.assertEqual(response.channels[0].capacity_msat,
                         (loc_bal + rem_bal) * 1000)
        self.assertEqual(response.channels[0].local_balance_msat,
                         loc_bal * 1000)
        self.assertEqual(response.channels[0].remote_balance_msat,
                         rem_bal * 1000)
        # Active: filled, initiator=True, remote balance above dust limit
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        MOD._add_channel(response,
                         fix.OPEN_CHAN_INITIATOR,
                         pb.Channel.OPEN,
                         open_chan=True)
        loc_bal = fix.OPEN_CHAN_INITIATOR.local_balance + \
            fix.OPEN_CHAN_INITIATOR.commit_fee
        rem_bal = fix.OPEN_CHAN_INITIATOR.remote_balance
        self.assertEqual(response.channels[0].local_balance_msat,
                         loc_bal * 1000)
        self.assertEqual(response.channels[0].remote_balance_msat,
                         rem_bal * 1000)
        # Active: filled, initiator=False, local balance below dust limit
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        MOD._add_channel(response,
                         fix.OPEN_CHAN_BELOW_DUST,
                         pb.Channel.OPEN,
                         open_chan=True)
        loc_bal = fix.OPEN_CHAN_BELOW_DUST.local_balance
        rem_bal = fix.OPEN_CHAN_BELOW_DUST.remote_balance + \
            fix.OPEN_CHAN_BELOW_DUST.commit_fee - \
            fix.OPEN_CHAN_BELOW_DUST.local_balance
        self.assertEqual(response.channels[0].local_balance_msat,
                         loc_bal * 1000)
        self.assertEqual(response.channels[0].remote_balance_msat,
                         rem_bal * 1000)
        # Active: filled, initiator=False, local balance above dust limit
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        MOD._add_channel(response,
                         fix.OPEN_CHAN,
                         pb.Channel.OPEN,
                         open_chan=True)
        loc_bal = fix.OPEN_CHAN.local_balance
        rem_bal = fix.OPEN_CHAN.remote_balance + fix.OPEN_CHAN.commit_fee
        self.assertEqual(response.channels[0].local_balance_msat,
                         loc_bal * 1000)
        self.assertEqual(response.channels[0].remote_balance_msat,
                         rem_bal * 1000)
        # WaitingCloseChannel: empty
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        pending_chan = ln.PendingChannelsResponse.PendingChannel()
        lnd_chan = ln.PendingChannelsResponse.WaitingCloseChannel(
            channel=pending_chan)
        MOD._add_channel(response, lnd_chan, pb.Channel.PENDING_MUTUAL_CLOSE)
        self.assertEqual(response, pb.ListChannelsResponse())
        # WaitingCloseChannel: filled, initiator=False
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        lnd_chan = fix.WAITING_CLOSE_CHAN
        MOD._add_channel(response, lnd_chan, pb.Channel.PENDING_MUTUAL_CLOSE)
        self.assertEqual(response.channels[0].remote_pubkey,
                         fix.REMOTE_NODE_PUB)
        self.assertEqual(response.channels[0].remote_balance_msat,
                         rem_bal * 1000)
        self.assertEqual(response.channels[0].local_balance_msat,
                         loc_bal * 1000)
        # PendingOpenChannel: filled, initiator=True
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        lnd_chan = fix.PENDING_OPEN_CHAN
        MOD._add_channel(response, lnd_chan, pb.Channel.PENDING_OPEN)
        self.assertEqual(response.channels[0].remote_pubkey,
                         fix.REMOTE_NODE_PUB)
        loc_bal = fix.PENDING_OPEN_CHAN.channel.local_balance + \
            fix.PENDING_OPEN_CHAN.commit_fee
        rem_bal = fix.PENDING_OPEN_CHAN.channel.remote_balance
        self.assertEqual(response.channels[0].local_balance_msat,
                         loc_bal * 1000)
        self.assertEqual(response.channels[0].remote_balance_msat,
                         rem_bal * 1000)
        # Skip add of inactive channel case
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        MOD._add_channel(response, fix.OPEN_CHAN_INACTIVE, pb.Channel.OPEN,
                         True)

    def test_check_timestamp(self):
        # list_order 1, search_order 1:
        # search_timestamp lower than creation_date
        request = pb.ListInvoicesRequest(search_timestamp=fix.NOW - 1,
                                         list_order=1,
                                         search_order=1)
        lnd_invoice = ln.Invoice(creation_date=fix.NOW)
        res = MOD._check_timestamp(request, lnd_invoice)
        self.assertEqual(res, True)
        # list_order 1, search_order 1:
        # search_timestamp higher than creation_date
        request = pb.ListInvoicesRequest(search_timestamp=fix.NOW + 1,
                                         list_order=1,
                                         search_order=1)
        lnd_invoice = ln.Invoice(creation_date=fix.NOW)
        res = MOD._check_timestamp(request, lnd_invoice)
        self.assertEqual(res, False)
        # list_order 1, search_order 0:
        # search_timestamp lower than creation_date
        request = pb.ListInvoicesRequest(search_timestamp=fix.NOW - 1,
                                         list_order=1,
                                         search_order=0)
        lnd_invoice = ln.Invoice(creation_date=fix.NOW)
        res = MOD._check_timestamp(request, lnd_invoice)
        self.assertEqual(res, False)
        # list_order 1, search_order 0:
        # search_timestamp higher than creation_date
        request = pb.ListInvoicesRequest(search_timestamp=fix.NOW + 1,
                                         list_order=1,
                                         search_order=0)
        lnd_invoice = ln.Invoice(creation_date=fix.NOW)
        res = MOD._check_timestamp(request, lnd_invoice)
        self.assertEqual(res, True)
        # list_order 0, search_order 1:
        # search_timestamp lower than creation_date
        request = pb.ListInvoicesRequest(search_timestamp=fix.NOW - 1,
                                         list_order=0,
                                         search_order=1)
        lnd_invoice = ln.Invoice(creation_date=fix.NOW)
        res = MOD._check_timestamp(request, lnd_invoice)
        self.assertEqual(res, True)
        # list_order 0, search_order 1:
        # search_timestamp higher than creation_date
        request = pb.ListInvoicesRequest(search_timestamp=fix.NOW + 1,
                                         list_order=0,
                                         search_order=1)
        lnd_invoice = ln.Invoice(creation_date=fix.NOW)
        res = MOD._check_timestamp(request, lnd_invoice)
        self.assertEqual(res, False)
        # list_order 0, search_order 0:
        # search_timestamp lower than creation_date
        request = pb.ListInvoicesRequest(search_timestamp=fix.NOW - 1,
                                         list_order=0,
                                         search_order=0)
        lnd_invoice = ln.Invoice(creation_date=fix.NOW)
        res = MOD._check_timestamp(request, lnd_invoice)
        self.assertEqual(res, False)
        # list_order 0, search_order 0:
        # search_timestamp higher than creation_date
        request = pb.ListInvoicesRequest(search_timestamp=fix.NOW + 1,
                                         list_order=0,
                                         search_order=0)
        lnd_invoice = ln.Invoice(creation_date=fix.NOW)
        res = MOD._check_timestamp(request, lnd_invoice)
        self.assertEqual(res, True)
        # Empty request
        request = pb.ListInvoicesRequest()
        res = MOD._check_timestamp(request, 'lnd_invoice')
        self.assertEqual(res, False)

    @patch(MOD.__name__ + '._add_invoice', autospec=True)
    @patch(MOD.__name__ + '._get_invoice_state', autospec=True)
    @patch(MOD.__name__ + '._check_timestamp', autospec=True)
    def test_parse_invoices(self, mocked_check, mocked_inv_st, mocked_add):
        # Correct case: every state is requested
        mocked_inv_st.side_effect = [
            pb.Invoice.PAID, pb.Invoice.PENDING, pb.Invoice.EXPIRED,
            pb.Invoice.UNKNOWN
        ]
        response = pb.ListInvoicesResponse()
        invoices = fix.INVOICES
        mocked_check.side_effect = [False] * len(invoices)
        request = pb.ListInvoicesRequest(paid=True,
                                         pending=True,
                                         expired=True,
                                         unknown=True,
                                         max_items=10)
        res = MOD._parse_invoices(response, invoices, request)
        self.assertEqual(res, False)
        self.assertEqual(mocked_add.call_count, len(invoices))
        # Correct case: no state is requested, should return all invoices
        reset_mocks(vars())
        mocked_inv_st.side_effect = [
            pb.Invoice.PAID, pb.Invoice.PENDING, pb.Invoice.EXPIRED,
            pb.Invoice.UNKNOWN
        ]
        response = pb.ListInvoicesResponse()
        request = pb.ListInvoicesRequest()
        mocked_check.side_effect = [False] * len(invoices)
        res = MOD._parse_invoices(response, invoices, request)
        self.assertEqual(res, True)
        # _check_timestamp true
        reset_mocks(vars())
        mocked_inv_st.side_effect = [
            pb.Invoice.PAID, pb.Invoice.PENDING, pb.Invoice.EXPIRED,
            pb.Invoice.UNKNOWN
        ]
        mocked_check.side_effect = [True] * len(invoices)
        res = MOD._parse_invoices(response, invoices, request)
        self.assertEqual(mocked_check.call_count, len(invoices))
        self.assertEqual(mocked_add.call_count, 0)
        self.assertEqual(res, False)

    @patch(MOD.__name__ + '._add_route_hint', autospec=True)
    def test_add_invoice(self, mocked_add):
        # Correct case
        response = pb.ListInvoicesResponse()
        MOD._add_invoice(response, fix.INVOICE, 2)
        calls = []
        for invoice in response.invoices:
            for route in fix.INVOICE.route_hints:
                calls.append(call(invoice, route))
        mocked_add.assert_has_calls(calls)
        self.assertEqual(response.invoices[0].description, fix.INVOICE.memo)
        self.assertEqual(response.invoices[0].amount_received_msat,
                         fix.INVOICE.amt_paid_msat)
        # Empty invoice
        reset_mocks(vars())
        response = pb.ListInvoicesResponse()
        invoice = ln.Invoice()
        MOD._add_invoice(response, invoice, 2)

    def test_add_payment(self):
        # Correct case
        response = pb.ListPaymentsResponse()
        MOD._add_payment(response, fix.PAYMENT)
        self.assertEqual(response.payments[0].payment_hash,
                         fix.PAYMENT.payment_hash)
        self.assertEqual(response.payments[0].amount_msat,
                         fix.PAYMENT.value_msat)
        # Empty payment
        reset_mocks(vars())
        response = pb.ListPaymentsResponse()
        payment = ln.Payment()
        MOD._add_payment(response, payment)

    def test_add_transaction(self):
        # Correct case
        response = pb.ListTransactionsResponse()
        MOD._add_transaction(response, fix.TRANSACTION)
        self.assertEqual(response.transactions[0].txid,
                         fix.TRANSACTION.tx_hash)
        self.assertEqual(response.transactions[0].amount_sat,
                         fix.TRANSACTION.amount)
        # Correct case: outgoing transaction
        reset_mocks(vars())
        response = pb.ListTransactionsResponse()
        MOD._add_transaction(response, fix.TRANSACTION_OUTGOING)
        self.assertEqual(response.transactions[0].amount_sat,
                         fix.TRANSACTION_OUTGOING.amount)
        # Empty transaction
        reset_mocks(vars())
        response = pb.ListTransactionsResponse()
        transaction = ln.Transaction()
        MOD._add_transaction(response, transaction)

    def test_add_route_hint(self):
        response = pb.DecodeInvoiceResponse()
        lnd_route = ln.RouteHint()
        # Empty
        MOD._add_route_hint(response, lnd_route)
        self.assertEqual(response, pb.DecodeInvoiceResponse())
        # Filled
        reset_mocks(vars())
        lnd_route.hop_hints.add(node_id='id', fee_base_msat=77)
        MOD._add_route_hint(response, lnd_route)
        self.assertEqual(response.route_hints[0].hop_hints[0].pubkey, 'id')

    @patch(MOD.__name__ + '.datetime', autospec=True)
    def test_get_invoice_state(self, mocked_datetime):
        mocked_datetime.now().timestamp.return_value = fix.NOW
        # Correct case: paid invoice
        lnd_invoice = ln.Invoice(state=ln.Invoice.SETTLED)
        res = MOD._get_invoice_state(lnd_invoice)
        self.assertEqual(res, pb.Invoice.PAID)
        # Correct case: unpaid invoice
        lnd_invoice = ln.Invoice(state=ln.Invoice.OPEN,
                                 creation_date=fix.NOW,
                                 expiry=77)
        res = MOD._get_invoice_state(lnd_invoice)
        self.assertEqual(res, pb.Invoice.PENDING)
        # Correct case: expired invoice
        lnd_invoice = ln.Invoice(state=ln.Invoice.OPEN,
                                 creation_date=fix.NOW - 78,
                                 expiry=77)
        res = MOD._get_invoice_state(lnd_invoice)
        self.assertEqual(res, pb.Invoice.EXPIRED)
        # Correct case: cancelled invoice
        lnd_invoice = ln.Invoice(state=ln.Invoice.CANCELED)
        res = MOD._get_invoice_state(lnd_invoice)
        self.assertEqual(res, pb.Invoice.EXPIRED)
        # Invoice with no status case
        reset_mocks(vars())
        lnd_invoice = fix.INVOICE_UNKNOWN
        res = MOD._get_invoice_state(lnd_invoice)
        self.assertEqual(res, pb.Invoice.UNKNOWN)

    @patch(MOD.__name__ + '.Err')
    def test_handle_error(self, mocked_err):
        # Error string
        res = MOD._handle_error(CTX, 'an error')
        self.assertIsNone(res)
        mocked_err().report_error.assert_called_with(CTX, 'an error')
        # RPC invocation: error object has details method
        reset_mocks(vars())
        error = fix.CalledRpcError()
        MOD._handle_error(CTX, error)
        mocked_err().report_error.assert_called_with(CTX, error.details())
        # Error doesn't have the details method and is not a string
        reset_mocks(vars())
        error = RpcError()
        MOD._handle_error(CTX, error)
        mocked_err().report_error.assert_called_with(
            CTX, 'Could not decode error message')

    def test_txid_bytes_to_str(self):
        btxid = (b'\366\2006?\300l0\361\351W\355\242\265qC#9qk\245c*t<\367'
                 b'\202\274,~\034U#')
        ltxid = ('23551c7e2cbc82f73c742a63a56b7139234371b5a2ed57e9f1306cc03f'
                 '3680f6')
        res = MOD._txid_bytes_to_str(btxid)
        self.assertEqual(res, ltxid)
