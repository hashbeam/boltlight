# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for utils.boltlight module."""

from importlib import import_module
from unittest import TestCase
from unittest.mock import Mock

from grpc import StatusCode

from .. import PROJ_ROOT

MOD = import_module(PROJ_ROOT + '.utils.boltlight')


class RuntimeTerminateTests(TestCase):
    """Tests for RuntimeTerminate helper class."""
    def test_missing_macaroon(self):
        ign_req = 'ignored_request'
        ctx = Mock()
        MOD.RuntimeTerminate.missing_macaroon(ign_req, ctx)
        ctx.abort.assert_called_once_with(StatusCode.UNAUTHENTICATED,
                                          'Macaroon authentication missing')

    def test_macaroon_error(self):
        ign_req = 'ignored_request'
        ctx = Mock()
        MOD.RuntimeTerminate.macaroon_error(ign_req, ctx)
        ctx.abort.assert_called_once_with(StatusCode.PERMISSION_DENIED,
                                          'Macaroon authentication error')

    def test_already_unlocked(self):
        ign_req = 'ignored_request'
        ctx = Mock()
        MOD.RuntimeTerminate.already_unlocked(ign_req, ctx)
        ctx.abort.assert_called_once_with(StatusCode.FAILED_PRECONDITION,
                                          'Boltlight is already unlocked')

    def test_not_runtime(self):
        ign_req = 'ignored_request'
        ctx = Mock()
        MOD.RuntimeTerminate.not_runtime(ign_req, ctx)
        ctx.abort.assert_called_once_with(StatusCode.UNIMPLEMENTED,
                                          'Not a runtime method')
