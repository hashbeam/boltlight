# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for utils.bitcoin module."""

from decimal import Decimal
from importlib import import_module
from unittest import TestCase
from unittest.mock import patch

from .. import PROJ_ROOT, reset_mocks
from . import fixtures_utils as fix

# pylint: disable=invalid-name, protected-access

CTX = 'context'
Enf = getattr(import_module(PROJ_ROOT + '.utils.bitcoin'), 'Enforcer')
pb = import_module(PROJ_ROOT + '.boltlight_pb2')
sett = import_module(PROJ_ROOT + '.settings')

MOD = import_module(PROJ_ROOT + '.utils.bitcoin')


class UtilsBitcoinTests(TestCase):
    """Tests for utils.bitcoin module."""
    @patch(MOD.__name__ + '.Enforcer.check_value')
    @patch(MOD.__name__ + '.Err')
    def test_convert(self, mocked_err, mocked_check_val):
        mocked_err().internal_value_error.side_effect = Exception()
        mocked_err().value_error.side_effect = Exception()
        # Correct case: float output
        res = MOD.convert(CTX, Enf.SATS, Enf.BITS, 777, Enf.MSATS)
        self.assertEqual(res, 7.77)
        self.assertEqual(type(res), float)
        assert not mocked_check_val.called
        assert not mocked_err().value_error.called
        # Correct case: int output with enforce
        reset_mocks(vars())
        res = MOD.convert(CTX, Enf.BITS, Enf.SATS, 777, Enf.SATS, Enf.OC_TX)
        mocked_check_val.asset_called_once()
        self.assertEqual(res, 77700)
        self.assertEqual(type(res), int)
        # Correct case: float output with enforce
        reset_mocks(vars())
        res = MOD.convert(CTX, Enf.BITS, Enf.BTC, 777, Enf.SATS, Enf.OC_TX)
        mocked_check_val.asset_called_once()
        self.assertEqual(res, 0.000777)
        self.assertEqual(type(res), float)
        # Correct case: None input
        reset_mocks(vars())
        res = MOD.convert(CTX, Enf.BITS, Enf.BTC, None, Enf.SATS, Enf.OC_TX)
        self.assertEqual(res, 0)
        # Error case: string input
        reset_mocks(vars())
        with self.assertRaises(Exception):
            res = MOD.convert(CTX, Enf.BITS, Enf.SATS, 'err', Enf.MSATS)
        mocked_err().internal_value_error.assert_called_once_with(CTX, 'err')
        # Error case: too big number input
        reset_mocks(vars())
        with self.assertRaises(Exception):
            res = MOD.convert(CTX, Enf.BITS, Enf.SATS,
                              777777777777777777777777777, Enf.SATS)
        mocked_err().value_error.assert_called_once_with(
            CTX, 777777777777777777777777777)
        # Error case: number gets truncated
        reset_mocks(vars())
        with self.assertRaises(Exception):
            res = MOD.convert(CTX, Enf.BITS, Enf.SATS, 0.009, Enf.SATS)
        mocked_err().value_error.assert_called_once_with(
            CTX, Decimal(str(0.009)))

    def test_get_address_type(self):
        # Bech32 address case
        addr = 'bcrt1q9s8pfy8ktptz2'
        res = MOD.get_address_type(addr)
        self.assertEqual(res, pb.Address.P2WPKH)
        addr = 'tb1qw508d6qejxtdg4y'
        res = MOD.get_address_type(addr)
        self.assertEqual(res, pb.Address.P2WPKH)
        # Legacy address case
        addr = 'm2gfudf487cn5acf284'
        res = MOD.get_address_type(addr)
        self.assertEqual(res, pb.Address.NP2WPKH)

    def test_get_channel_balances(self):
        # Full channel list case
        channels = fix.LISTCHANNELRESPONSE.channels
        res = MOD.get_channel_balances(channels)
        self.assertEqual(res.out_tot_msat, 1450100)
        self.assertEqual(res.out_tot_now_msat, 678100)
        self.assertEqual(res.in_tot_msat, 858700)
        self.assertEqual(res.in_tot_now_msat, 213700)
        # Empty channel list case
        reset_mocks(vars())
        res = MOD.get_channel_balances([])
        self.assertEqual(res, pb.BalanceOffChainResponse())

    @patch(MOD.__name__ + '._has_numbers', autospec=True)
    def test_has_amount_encoded(self, mocked_has_num):
        pay_req = 'lntb5n1pw3mupk'
        mocked_has_num.return_value = True
        res = MOD.has_amount_encoded(pay_req)
        self.assertEqual(res, True)
        pay_req = 'lntb1pw3mumupk'
        mocked_has_num.return_value = False
        res = MOD.has_amount_encoded(pay_req)
        self.assertEqual(res, False)

    @patch(MOD.__name__ + '.Err')
    def test_split_node_uri(self, mocked_err):
        mocked_err().invalid.side_effect = Exception()
        # Correct case
        pubkey, host_addr = MOD.split_node_uri(CTX, fix.NODE_URI)
        self.assertEqual(pubkey, fix.NODE_ID)
        self.assertEqual(host_addr, fix.HOST_ADDRESS)
        # Invalid node_uri case
        with self.assertRaises(Exception):
            MOD.split_node_uri(CTX, 'not a node uri@')
        mocked_err().invalid.assert_called_once_with(CTX, 'node_uri')

    def test_has_numbers(self):
        res = MOD._has_numbers('light3r')
        self.assertEqual(res, True)
        res = MOD._has_numbers('boltlight')
        self.assertEqual(res, False)

    @patch(MOD.__name__ + '.Err')
    def test_check_value(self, mocked_err):
        mocked_err().value_too_low.side_effect = Exception()
        mocked_err().value_too_high.side_effect = Exception()
        # Correct case, default type
        Enf.check_value(CTX, 7)
        # Correct case, specific type
        Enf.check_value(CTX, 7, enforce=Enf.LN_TX)
        # Error value_too_low case
        reset_mocks(vars())
        with self.assertRaises(Exception):
            Enf.check_value(CTX, 0.001, enforce=Enf.LN_TX)
        mocked_err().value_too_low.assert_called_once_with(CTX, 0.001)
        assert not mocked_err().value_too_high.called
        # Error value_too_high case
        reset_mocks(vars())
        with self.assertRaises(Exception):
            Enf.check_value(CTX, 2**32 + 1, enforce=Enf.LN_TX)
        assert not mocked_err().value_too_low.called
        mocked_err().value_too_high.assert_called_once_with(CTX, 2**32 + 1)
        # Check disabled case
        reset_mocks(vars())
        sett.ENFORCE = False
        Enf.check_value(CTX, 7)
        assert not mocked_err().value_too_low.called
        assert not mocked_err().value_too_high.called
