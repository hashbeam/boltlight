# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for light_eclair module."""

from concurrent.futures import TimeoutError as TimeoutFutError
from importlib import import_module
from unittest import TestCase
from unittest.mock import Mock, call, patch

from .. import PROJ_ROOT, reset_mocks
from . import fixtures_eclair as fix

# pylint: disable=invalid-name, protected-access, too-many-arguments
# pylint: disable=too-many-locals, too-many-public-methods, too-many-statements

CTX = 'context'
Enf = getattr(import_module(PROJ_ROOT + '.utils.bitcoin'), 'Enforcer')
FakeContext = getattr(import_module(PROJ_ROOT + '.utils.network'),
                      'FakeContext')
pb = import_module(PROJ_ROOT + '.boltlight_pb2')
sett = import_module(PROJ_ROOT + '.settings')

MOD = import_module(PROJ_ROOT + '.light_eclair')


class LightEclairTests(TestCase):
    """Tests for light_eclair module."""
    @patch(MOD.__name__ + '.EclairRPC')
    def test_get_node_version(self, mocked_rpcses):
        ses = mocked_rpcses.return_value
        # node answers case
        ses.getinfo.return_value = fix.GETINFO_MAINNET, False
        res = MOD.get_node_version()
        self.assertEqual(res, fix.GETINFO_MAINNET['version'])
        # node returns error case (is_err=True)
        ses.getinfo.return_value = fix.BADRESPONSE, True
        res = MOD.get_node_version()
        self.assertEqual(res, '')
        # node is not responsive case
        ses.getinfo.side_effect = RuntimeError
        res = MOD.get_node_version()
        self.assertEqual(res, '')

    @patch(MOD.__name__ + '.set_defaults', autospec=True)
    def test_get_settings(self, mocked_set_def):
        # Correct case
        ecl_host = 'eclair'
        ecl_port = '8080'
        config = Mock()
        config.get.side_effect = [ecl_host, ecl_port]
        MOD.get_settings(config, 'eclair')
        ecl_values = ['ECL_HOST', 'ECL_PORT']
        mocked_set_def.assert_called_once_with(config, ecl_values)
        self.assertEqual(sett.IMPL_SEC_TYPE, 'password')
        self.assertEqual(sett.RPC_URL, f'http://{ecl_host}:{ecl_port}')

    def test_update_settings(self):
        password = b'password'
        # Correct case
        MOD.update_settings(password)
        self.assertEqual(sett.ECL_PASS, password.decode())

    def test_unlock_node(self):
        res = MOD.unlock_node(CTX, 'whatever')
        self.assertEqual(res, pb.UnlockNodeResponse())

    @patch(MOD.__name__ + '.unlock_node', autospec=True)
    def test_UnlockNode(self, mocked_unlock):
        res = MOD.UnlockNode(pb.UnlockNodeRequest(), CTX)
        self.assertEqual(res, mocked_unlock.return_value)
        mocked_unlock.assert_called_once_with(None, None)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    def test_GetNodeInfo(self, mocked_rpcses, mocked_handle):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        # Mainnet case
        reset_mocks(vars())
        ses.getinfo.return_value = (fix.GETINFO_MAINNET, False)
        res = MOD.GetNodeInfo('request', CTX)
        ses.getinfo.assert_called_once_with(CTX)
        self.assertEqual(res.network, pb.Network.MAINNET)
        # Regtest case
        reset_mocks(vars())
        ses.getinfo.return_value = (fix.GETINFO_UNKNOWN, False)
        res = MOD.GetNodeInfo('request', CTX)
        ses.getinfo.assert_called_once_with(CTX)
        self.assertEqual(res.network, pb.Network.REGTEST)
        # Testnet case
        reset_mocks(vars())
        ses.getinfo.return_value = (fix.GETINFO_TESTNET, False)
        res = MOD.GetNodeInfo('request', CTX)
        ses.getinfo.assert_called_once_with(CTX)
        self.assertEqual(res.network, pb.Network.TESTNET)
        self.assertEqual(res.identity_pubkey, fix.GETINFO_TESTNET['nodeId'])
        self.assertEqual(res.alias, fix.GETINFO_TESTNET['alias'])
        self.assertEqual(res.block_height, fix.GETINFO_TESTNET['blockHeight'])
        self.assertEqual(res.color, fix.GETINFO_TESTNET['color'])
        # Error case
        reset_mocks(vars())
        ses.getinfo.return_value = (fix.ERR, True)
        with self.assertRaises(Exception):
            res = MOD.GetNodeInfo('request', CTX)
        mocked_handle.assert_called_once_with(CTX, fix.ERR)

    @patch(MOD.__name__ + '.get_address_type', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    def test_NewAddress(self, mocked_rpcses, mocked_handle, mocked_err,
                        mocked_addr_type):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        mocked_err().unimplemented_param_value.side_effect = Exception()
        # Segwit case: addr_type = 0 = P2WPKH = NATIVE_SEGWIT
        mocked_addr_type.return_value = pb.Address.P2WPKH
        ses.getnewaddress.return_value = fix.ADDRESS_SEGWIT, False
        req = pb.NewAddressRequest()
        res = MOD.NewAddress(req, CTX)
        self.assertEqual(res.address, fix.ADDRESS_SEGWIT)
        mocked_addr_type.assert_called_once_with(fix.ADDRESS_SEGWIT)
        # Legacy case: addr_type = 1 = NP2WPKH = NESTED_SEGWIT
        reset_mocks(vars())
        mocked_addr_type.return_value = pb.Address.NP2WPKH
        ses.getnewaddress.return_value = fix.ADDRESS_P2SH_SEGWIT, False
        req = pb.NewAddressRequest(addr_type=pb.Address.NP2WPKH)
        res = MOD.NewAddress(req, CTX)
        self.assertEqual(res.address, fix.ADDRESS_P2SH_SEGWIT)
        mocked_addr_type.assert_called_once_with(fix.ADDRESS_P2SH_SEGWIT)
        # Unsupported address type
        reset_mocks(vars())
        ses.getnewaddress.return_value = fix.ADDRESS_SEGWIT, False
        mocked_addr_type.return_value = pb.Address.P2WPKH
        req = pb.NewAddressRequest(addr_type=pb.Address.NP2WPKH)
        with self.assertRaises(Exception):
            res = MOD.NewAddress(req, CTX)
        mocked_err().unimplemented_param_value.assert_called_once_with(
            CTX, 'addr_type', pb.Address.Type.Name(1))
        # Error case
        reset_mocks(vars())
        err_msg = 'error'
        ses.getnewaddress.return_value = err_msg, True
        with self.assertRaises(Exception):
            res = MOD.NewAddress(req, CTX)
        mocked_handle.assert_called_once_with(CTX, err_msg)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    def test_BalanceOnChain(self, mocked_rpcses, mocked_handle):
        ses = mocked_rpcses.return_value
        ses.onchainbalance.return_value = fix.ONCHAINBALANCE, False
        mocked_handle.side_effect = Exception()
        # Correct case
        res = MOD.BalanceOnChain('req', CTX)
        self.assertEqual(res.confirmed_sat, fix.ONCHAINBALANCE['confirmed'])
        self.assertEqual(
            res.total_sat, fix.ONCHAINBALANCE['confirmed'] +
            fix.ONCHAINBALANCE['unconfirmed'])
        # Correct case: with no unconfirmed balance
        reset_mocks(vars())
        ses.onchainbalance.return_value = fix.ONCHAINBALANCE_NO_UNCONFIRMED, False
        res = MOD.BalanceOnChain('req', CTX)
        self.assertEqual(res.confirmed_sat,
                         fix.ONCHAINBALANCE_NO_UNCONFIRMED['confirmed'])
        self.assertEqual(
            res.total_sat, fix.ONCHAINBALANCE_NO_UNCONFIRMED['confirmed'] +
            fix.ONCHAINBALANCE_NO_UNCONFIRMED['unconfirmed'])
        # No balance case
        reset_mocks(vars())
        ses.onchainbalance.return_value = fix.ONCHAINBALANCE_EMPTY, False
        res = MOD.BalanceOnChain('req', CTX)
        # Error case
        reset_mocks(vars())
        err_msg = 'error'
        ses.onchainbalance.return_value = err_msg, True
        with self.assertRaises(Exception):
            res = MOD.BalanceOnChain('req', CTX)
        mocked_handle.assert_called_once_with(CTX, err_msg)

    @patch(MOD.__name__ + '.get_channel_balances', autospec=True)
    @patch(MOD.__name__ + '.ListChannels', autospec=True)
    def test_BalanceOffChain(self, mocked_ListChannels, mocked_get_chan_bal):
        mocked_get_chan_bal.return_value = pb.BalanceOffChainResponse()
        res = MOD.BalanceOffChain('request', CTX)
        assert mocked_ListChannels.called
        self.assertEqual(res, pb.BalanceOffChainResponse())

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    def test_ListPeers(self, mocked_rpcses, mocked_handle):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        # Correct case
        ses.peers.return_value = (fix.PEERS, False)
        ses.nodes.return_value = (fix.ALLNODES, False)
        res = MOD.ListPeers('request', CTX)
        ses.peers.assert_called_once_with(CTX)
        ses.nodes.assert_called_once_with(CTX)
        self.assertEqual(res.peers[0].pubkey, fix.PEERS[0]['nodeId'])
        # Empty case
        reset_mocks(vars())
        ses.peers.return_value = ([], False)
        res = MOD.ListPeers('request', CTX)
        self.assertEqual(res, pb.ListPeersResponse())
        # peers error case
        reset_mocks(vars())
        ses.peers.return_value = (fix.ERR, True)
        with self.assertRaises(Exception):
            res = MOD.ListPeers('request', CTX)
        mocked_handle.assert_called_once_with(CTX, fix.ERR)
        # nodes error case (ignoring error)
        reset_mocks(vars())
        ses.peers.return_value = (fix.PEERS, False)
        ses.nodes.return_value = (fix.ERR, True)
        res = MOD.ListPeers('request', CTX)
        assert not mocked_handle.called

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '._add_channel', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    def test_ListChannels(self, mocked_rpcses, mocked_add, mocked_handle):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        # List all channels
        reset_mocks(vars())
        ses.channels.return_value = (fix.CHANNELS, False)
        request = pb.ListChannelsRequest(active_only=False)
        res = MOD.ListChannels(request, CTX)
        ses.channels.assert_called_once_with(CTX)
        calls = [
            call(pb.ListChannelsResponse(), fix.CHANNEL_NORMAL, False),
            call(pb.ListChannelsResponse(), fix.CHANNEL_WAITING_FUNDING, False)
        ]
        mocked_add.assert_has_calls(calls)
        self.assertEqual(res, pb.ListChannelsResponse())
        # Error case
        reset_mocks(vars())
        ses.channels.return_value = (fix.ERR, True)
        with self.assertRaises(Exception):
            res = MOD.ListChannels('request', CTX)
        ses.channels.assert_called_once_with(CTX)
        mocked_handle.assert_called_once_with(CTX, fix.ERR)
        assert not mocked_add.called

    @patch(MOD.__name__ + '._add_transaction', autospec=True)
    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    def test_ListTransactions(self, mocked_rpcses, mocked_handle, mocked_add):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        response = pb.ListTransactionsResponse()
        # Correct case
        request = pb.ListTransactionsRequest()
        ses.onchaintransactions.return_value = fix.ONCHAINTRANSACTIONS, False
        res = MOD.ListTransactions(request, CTX)
        calls = []
        for ele_transaction in fix.ONCHAINTRANSACTIONS:
            calls.append(call(response, ele_transaction))
        mocked_add.assert_has_calls(calls)
        # Error case
        reset_mocks(vars())
        ele_res = "Strange string"
        ses.onchaintransactions.return_value = ele_res, True
        with self.assertRaises(Exception):
            res = MOD.ListTransactions(request, CTX)
        assert not mocked_add.called
        mocked_handle.assert_called_once_with(CTX, ele_res)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    @patch(MOD.__name__ + '.Enf.check_value')
    @patch(MOD.__name__ + '.Err')
    def test_CreateInvoice(self, mocked_err, mocked_check, mocked_rpcses,
                           mocked_handle):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        mocked_err().unimplemented_parameter.side_effect = Exception()
        mocked_err().unsettable.side_effect = Exception()
        # Correct case
        reset_mocks(vars())
        req = pb.CreateInvoiceRequest(description='description',
                                      amount_msat=7,
                                      expiry=1,
                                      fallback_addr='fallback_addr')
        ses.createinvoice.return_value = (fix.CREATEINVOICE, False)
        res = MOD.CreateInvoice(req, CTX)
        mocked_check.assert_called_once_with(CTX,
                                             req.amount_msat,
                                             enforce=Enf.LN_PAYREQ)
        node_req = {
            'description': req.description,
            'amountMsat': req.amount_msat,
            'expireIn': req.expiry,
            'fallbackAddress': req.fallback_addr
        }
        ses.createinvoice.assert_called_once_with(CTX, node_req)
        self.assertEqual(res.payment_request, fix.CREATEINVOICE['serialized'])
        self.assertEqual(res.payment_hash, fix.CREATEINVOICE['paymentHash'])
        self.assertEqual(
            res.expires_at,
            fix.CREATEINVOICE['timestamp'] + fix.CREATEINVOICE['expiry'])
        # Empty request case
        reset_mocks(vars())
        req = pb.CreateInvoiceRequest()
        res = MOD.CreateInvoice(req, CTX)
        node_req = {'description': '', 'expireIn': sett.EXPIRY_TIME}
        ses.createinvoice.assert_called_once_with(CTX, node_req)
        # Error case
        reset_mocks(vars())
        req = pb.CreateInvoiceRequest()
        ses.createinvoice.return_value = (fix.ERR, True)
        with self.assertRaises(Exception):
            MOD.CreateInvoice(req, CTX)
        mocked_handle.assert_called_with(CTX, fix.ERR)
        # Unimplemented parameter case
        reset_mocks(vars())
        req = pb.CreateInvoiceRequest(min_final_cltv_expiry=7)
        with self.assertRaises(Exception):
            MOD.CreateInvoice(req, CTX)
        mocked_err().unimplemented_parameter.assert_called_once_with(
            CTX, 'min_final_cltv_expiry')
        assert not mocked_handle.called

    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '._get_invoice_state', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_CheckInvoice(self, mocked_check_par, mocked_rpcses, mocked_inv_st,
                          mocked_err):
        ses = mocked_rpcses.return_value
        pay_hash = 'payment_hash'
        mocked_err().invalid.side_effect = Exception()
        # Correct case
        mocked_inv_st.return_value = pb.Invoice.PAID
        request = pb.CheckInvoiceRequest(payment_hash=pay_hash)
        ses.getreceivedinfo.return_value = (fix.GETRECEIVEDINFO_PAID, False)
        res = MOD.CheckInvoice(request, CTX)
        ses.getreceivedinfo.assert_called_once_with(CTX,
                                                    {'paymentHash': pay_hash})
        assert not mocked_err().invalid.called
        self.assertEqual(res.state, mocked_inv_st.return_value)
        # Missing parameter case
        reset_mocks(vars())
        request = pb.CheckInvoiceRequest()
        mocked_check_par.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.CheckInvoice(request, CTX)
        mocked_check_par.side_effect = None
        # Error case
        reset_mocks(vars())
        ses.getreceivedinfo.return_value = (fix.ERR, True)
        with self.assertRaises(Exception):
            res = MOD.CheckInvoice(request, CTX)
        mocked_err().invalid.assert_called_once_with(CTX, 'payment_hash')

    @patch(MOD.__name__ + '.sleep', autospec=True)
    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    @patch(MOD.__name__ + '.Enf.check_value')
    @patch(MOD.__name__ + '.has_amount_encoded', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_PayInvoice(self, mocked_check_par, mocked_err, mocked_has_amt,
                        mocked_check_val, mocked_rpcses, mocked_handle,
                        mocked_sleep):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        mocked_err().unimplemented_parameter.side_effect = Exception()
        mocked_err().invalid.side_effect = Exception()
        mocked_err().unsettable.side_effect = Exception()
        mocked_err().payinvoice_failed.side_effect = Exception()
        # Correct case: with amount requested
        req = pb.PayInvoiceRequest(payment_request='pay_req', amount_msat=777)
        mocked_has_amt.return_value = False
        ses.payinvoice.return_value = (fix.PAYINVOICE, False)
        ctx = FakeContext()
        res = MOD.PayInvoice(req, ctx)
        mocked_check_val.assert_called_once_with(ctx, req.amount_msat,
                                                 Enf.LN_TX)
        node_req = {
            'invoice': req.payment_request,
            'blocking': True,
            'amountMsat': req.amount_msat
        }
        ses.payinvoice.assert_called_once_with(ctx, node_req)
        self.assertEqual(res.payment_preimage,
                         fix.PAYINVOICE['paymentPreimage'])
        # Correct case: no amount requested
        reset_mocks(vars())
        req = pb.PayInvoiceRequest(payment_request='payment_req')
        mocked_has_amt.return_value = False
        res = MOD.PayInvoice(req, ctx)
        node_req = {'invoice': req.payment_request, 'blocking': True}
        ses.payinvoice.assert_called_once_with(ctx, node_req)
        # Missing parameter payment_request case
        reset_mocks(vars())
        req = pb.PayInvoiceRequest()
        mocked_check_par.side_effect = [Exception(), None]
        with self.assertRaises(Exception):
            MOD.PayInvoice(req, ctx)
        mocked_check_par.assert_called_once_with(ctx, req, 'payment_request')
        # Unimplemented parameter case
        reset_mocks(vars())
        mocked_check_par.side_effect = None
        req = pb.PayInvoiceRequest(cltv_expiry_delta=7)
        with self.assertRaises(Exception):
            MOD.PayInvoice(req, ctx)
        mocked_err().unimplemented_parameter.assert_called_once_with(
            ctx, 'cltv_expiry_delta')
        # Unsettable parameter amount_msat case
        reset_mocks(vars())
        req = pb.PayInvoiceRequest(payment_request='pay_req', amount_msat=777)
        mocked_has_amt.return_value = True
        with self.assertRaises(Exception):
            MOD.PayInvoice(req, ctx)
        mocked_err().unsettable.assert_called_once_with(ctx, 'amount_msat')
        # Missing parameter amount_msat case
        reset_mocks(vars())
        mocked_check_par.side_effect = [None, Exception()]
        req = pb.PayInvoiceRequest(payment_request='payment_request')
        mocked_has_amt.return_value = False
        with self.assertRaises(Exception):
            MOD.PayInvoice(req, ctx)
        self.assertEqual(mocked_check_par.call_count, 2)
        mocked_check_par.side_effect = None
        # Parameter payment_request not valid case
        reset_mocks(vars())
        mocked_has_amt.return_value = False
        ses.payinvoice.return_value = (fix.PAYINVOICE_MALFORMED, True)
        with self.assertRaises(Exception):
            MOD.PayInvoice(req, ctx)
        mocked_err().invalid.assert_called_once_with(ctx, req.payment_request)
        # Payment failed case
        reset_mocks(vars())
        mocked_has_amt.return_value = False
        ses.payinvoice.return_value = (fix.PAYINVOICE_FAIL, False)
        with self.assertRaises(Exception):
            MOD.PayInvoice(req, ctx)
        mocked_handle.assert_called_once_with(ctx, fix.PAYINVOICE_FAIL)
        # Missing payment preimage case
        reset_mocks(vars())
        ses.payinvoice.return_value = (fix.ERR, False)
        with self.assertRaises(Exception):
            MOD.PayInvoice(req, ctx)
        mocked_err().payinvoice_failed.assert_called_once_with(ctx)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    @patch(MOD.__name__ + '.Enf.check_value')
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_PayOnChain(self, mocked_check_par, mocked_err, mocked_check_val,
                        mocked_rpcses, mocked_handle):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        mocked_err().unimplemented_parameter.side_effect = Exception()
        amt = 7
        fee_sat_byte = 1
        # Missing parameter case
        request = pb.PayOnChainRequest()
        mocked_check_par.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.PayOnChain(request, CTX)
        # Unsupported fee case
        reset_mocks(vars())
        mocked_check_par.side_effect = None
        request = pb.PayOnChainRequest(amount_sat=amt,
                                       address=fix.ADDRESS,
                                       fee_sat_byte=fee_sat_byte)
        mocked_check_val.return_value = False
        with self.assertRaises(Exception):
            MOD.PayOnChain(request, CTX)
        assert not ses.sendonchain.called
        # Correct case
        reset_mocks(vars())
        request = pb.PayOnChainRequest(amount_sat=amt, address=fix.ADDRESS)
        mocked_check_val.return_value = True
        ses.sendonchain.return_value = fix.SENDONCHAIN, False
        MOD.PayOnChain(request, CTX)
        # Error case
        reset_mocks(vars())
        request = pb.PayOnChainRequest(address=fix.ADDRESS, amount_sat=amt)
        ses.sendonchain.return_value = fix.BADRESPONSE, True
        with self.assertRaises(Exception):
            res = MOD.PayOnChain(request, CTX)
        mocked_handle.assert_called_once_with(CTX, fix.BADRESPONSE)

    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '._is_description_hash', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_DecodeInvoice(self, mocked_check_par, mocked_err, mocked_rpcses,
                           mocked_d_hash, mocked_handle):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        mocked_err().invalid.side_effect = Exception()
        mocked_err().unimplemented_parameter.side_effect = Exception()
        # Correct case: with description hash
        req = pb.DecodeInvoiceRequest(payment_request='pay_req')
        ses.parseinvoice.return_value = (fix.PARSEINVOICE_D_HASH, False)
        mocked_d_hash.return_value = True
        res = MOD.DecodeInvoice(req, CTX)
        node_req = {'invoice': req.payment_request}
        ses.parseinvoice.assert_called_once_with(CTX, node_req)
        self.assertEqual(res.amount_msat,
                         int(fix.PARSEINVOICE_D_HASH['amount']))
        self.assertEqual(res.timestamp, fix.PARSEINVOICE_D_HASH['timestamp'])
        self.assertEqual(res.destination_pubkey,
                         fix.PARSEINVOICE_D_HASH['nodeId'])
        self.assertEqual(res.payment_hash,
                         fix.PARSEINVOICE_D_HASH['paymentHash'])
        self.assertEqual(res.description, '')
        self.assertEqual(res.description_hash,
                         fix.PARSEINVOICE_D_HASH['description'])
        self.assertEqual(res.expiry, 0)
        self.assertEqual(res.min_final_cltv_expiry, 0)
        # Correct case: with simple description
        reset_mocks(vars())
        mocked_d_hash.return_value = False
        req = pb.DecodeInvoiceRequest(payment_request='random')
        ses.parseinvoice.return_value = (fix.PARSEINVOICE, False)
        res = MOD.DecodeInvoice(req, CTX)
        self.assertEqual(res.amount_msat, 0)
        self.assertEqual(res.timestamp, fix.PARSEINVOICE['timestamp'])
        self.assertEqual(res.destination_pubkey, fix.PARSEINVOICE['nodeId'])
        self.assertEqual(res.payment_hash, fix.PARSEINVOICE['paymentHash'])
        self.assertEqual(res.description, fix.PARSEINVOICE['description'])
        self.assertEqual(res.description_hash, '')
        # Missing parameter case
        reset_mocks(vars())
        mocked_check_par.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.DecodeInvoice(req, CTX)
        mocked_check_par.assert_called_once_with(CTX, req, 'payment_request')
        # Incorrect invoice case
        reset_mocks(vars())
        mocked_check_par.side_effect = None
        req = pb.DecodeInvoiceRequest(payment_request='pay_req')
        ses.parseinvoice.return_value = ('aa invalid payment request zz', True)
        with self.assertRaises(Exception):
            MOD.DecodeInvoice(req, CTX)
        mocked_err().invalid.assert_called_once_with(CTX, 'payment_request')
        # Unimplemented parameter description case
        reset_mocks(vars())
        req = pb.DecodeInvoiceRequest(description='d')
        with self.assertRaises(Exception):
            MOD.DecodeInvoice(req, CTX)
        mocked_err().unimplemented_parameter.assert_called_once_with(
            CTX, 'description')
        # Error case
        reset_mocks(vars())
        ses.parseinvoice.return_value = (fix.ERR, True)
        req = pb.DecodeInvoiceRequest(payment_request='pay_req')
        with self.assertRaises(Exception):
            MOD.DecodeInvoice(req, CTX)
        mocked_handle.assert_called_once_with(CTX, fix.ERR)

    @patch(MOD.__name__ + '.Enf.check_value')
    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.split_node_uri', autospec=True)
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_OpenChannel(self, mocked_check_par, mocked_split, mocked_err,
                         mocked_rpcses, mocked_handle, mocked_check_val):
        ses = mocked_rpcses.return_value
        mocked_handle.side_effect = Exception()
        mocked_err().connect_failed.side_effect = Exception()
        mocked_err().value_too_high.side_effect = Exception()
        mocked_handle.side_effect = Exception()
        mocked_split.return_value = fix.NODE_URI.split('@')
        # Filled
        request = pb.OpenChannelRequest(funding_sat=7,
                                        node_uri=fix.NODE_URI,
                                        push_msat=77,
                                        private=True)
        ses.connect.return_value = (fix.CONNECT, False)
        ses.open.return_value = (fix.OPEN, False)
        ses.channel.return_value = (fix.CHANNEL_NORMAL, False)
        res = MOD.OpenChannel(request, CTX)
        outpoint = fix.CHANNEL_NORMAL['data']['commitments']['commitInput'][
            'outPoint']
        self.assertEqual(res.funding_txid, outpoint.split(':')[0])
        # Error: push amount too high
        reset_mocks(vars())
        request.push_msat = 1000 * request.funding_sat
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        mocked_err().value_too_high.assert_called_once_with(
            CTX, request.push_msat)
        request.push_msat = 77
        # Error in opening channel case
        reset_mocks(vars())
        request.private = False
        request.push_msat = 100
        ses.open.return_value = (fix.ERR, True)
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        mocked_handle.assert_called_once_with(CTX, fix.ERR)
        # Connect failed case
        reset_mocks(vars())
        ses.connect.return_value = (fix.ERR, True)
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        mocked_err().connect_failed.assert_called_once_with(CTX)
        # invalid node_uri case
        reset_mocks(vars())
        request.node_uri = 'wrong'
        mocked_split.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        assert not mocked_rpcses.called
        assert not ses.connect.called
        mocked_split.side_effect = None
        mocked_split.return_value = fix.NODE_URI.split('@')
        # Missing parameter case
        reset_mocks(vars())
        request = pb.OpenChannelRequest()
        mocked_check_par.side_effect = Exception()
        with self.assertRaises(Exception):
            MOD.OpenChannel(request, CTX)
        assert not ses.connect.called
        mocked_check_par.side_effect = None
        # Error in retrieving channel info (should not happen)
        reset_mocks(vars())
        request = pb.OpenChannelRequest(funding_sat=7, node_uri=fix.NODE_URI)
        ses.connect.return_value = (fix.CONNECT, False)
        ses.open.return_value = (fix.ERROR_CHANNEL, False)
        res = MOD.OpenChannel(request, CTX)
        self.assertEqual(res, pb.OpenChannelResponse())

    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '._handle_error', autospec=True)
    @patch(MOD.__name__ + '.get_thread_timeout', autospec=True)
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '.ThreadPoolExecutor', autospec=True)
    @patch(MOD.__name__ + '.check_req_params', autospec=True)
    def test_CloseChannel(self, mocked_check_par, mocked_thread,
                          mocked_get_time, mocked_thread_time, mocked_handle,
                          mocked_err):
        mocked_handle.side_effect = Exception()
        mocked_err().report_error.side_effect = Exception()
        mocked_get_time.return_value = 30
        mocked_thread_time.return_value = 2
        txid = 'txid'
        # Correct case
        future = Mock()
        executor = Mock()
        future.result.return_value = txid
        executor.submit.return_value = future
        mocked_thread.return_value.__enter__.return_value = executor
        request = pb.CloseChannelRequest(channel_id='777', force=True)
        ctx = Mock()
        ctx.time_remaining.return_value = 10
        res = MOD.CloseChannel(request, ctx)
        self.assertEqual(res.closing_txid, txid)
        mocked_check_par.assert_called_once_with(ctx, request, 'channel_id')
        # Result times out
        reset_mocks(vars())
        future.result.side_effect = TimeoutFutError()
        res = MOD.CloseChannel(request, ctx)
        executor.shutdown.assert_called_once_with(wait=False)
        self.assertEqual(res, pb.CloseChannelResponse())
        # Result throws RuntimeError
        reset_mocks(vars())
        future.result.side_effect = RuntimeError(fix.BADRESPONSE)
        with self.assertRaises(Exception):
            MOD.CloseChannel(request, ctx)
        mocked_handle.assert_called_once_with(ctx, fix.BADRESPONSE)
        # literal_eval throws SyntaxError
        reset_mocks(vars())
        err = 'err'
        future.result.side_effect = RuntimeError(err)
        with self.assertRaises(Exception):
            MOD.CloseChannel(request, ctx)
        assert not mocked_handle.called
        mocked_err().report_error.assert_called_once_with(ctx, err)
        future.result.side_effect = None
        # Bad Request
        reset_mocks(vars())
        err = 'Bad Request'
        future.result.side_effect = RuntimeError(err)
        with self.assertRaises(Exception):
            MOD.CloseChannel(request, ctx)
        assert not mocked_handle.called
        mocked_err().invalid.assert_called_once_with(ctx, 'channel_id')
        future.result.side_effect = None

    def test_is_description_hash(self):
        res = MOD._is_description_hash(fix.PARSEINVOICE_D_HASH['description'])
        self.assertEqual(res, True)
        res = MOD._is_description_hash(fix.PARSEINVOICE['description'])
        self.assertEqual(res, False)

    @patch(MOD.__name__ + '._get_channel_state', autospec=True)
    def test_add_channel(self, mocked_state):
        # Add channel case
        response = pb.ListChannelsResponse()
        mocked_state.return_value = pb.Channel.OPEN
        res = MOD._add_channel(response, fix.CHANNEL_NORMAL, False)
        spec = fix.CHANNEL_NORMAL['data']['commitments']['localCommit']['spec']
        calls = [
            call(CTX, Enf.MSATS, spec['toLocal']),
            call(CTX, Enf.MSATS, spec['toRemote'])
        ]
        self.assertEqual(res, None)
        self.assertEqual(response.channels[0].remote_pubkey,
                         fix.CHANNEL_NORMAL['nodeId'])
        self.assertEqual(response.channels[0].channel_id,
                         fix.CHANNEL_NORMAL['channelId'])
        self.assertEqual(response.channels[0].short_channel_id,
                         fix.CHANNEL_NORMAL['data']['shortChannelId'])
        self.assertEqual(response.channels[0].local_balance_msat, 227599428)
        self.assertEqual(response.channels[0].remote_balance_msat, 2400572)
        self.assertEqual(response.channels[0].capacity_msat, 230000000)
        self.assertEqual(response.channels[0].private, True)
        # Skip add of closed channel case
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        mocked_state.return_value = -1
        res = MOD._add_channel(response, fix.CHANNEL_NORMAL, False)
        self.assertEqual(response, pb.ListChannelsResponse())
        # Skip add of inactive channel case
        reset_mocks(vars())
        response = pb.ListChannelsResponse()
        mocked_state.return_value = pb.Channel.OPEN
        res = MOD._add_channel(response, fix.CHANNEL_OFFLINE, True)
        self.assertEqual(response, pb.ListChannelsResponse())
        # Add channel without commitments
        reset_mocks(vars())
        res = MOD._add_channel(response, fix.CHANNEL_UNKNOWN, False)
        self.assertNotEqual(response, pb.ListChannelsResponse())
        self.assertEqual(response.channels[0].short_channel_id,
                         fix.CHANNEL_UNKNOWN['data']['shortChannelId'])

    def test_add_transaction(self):
        # Correct case
        response = pb.ListTransactionsResponse()
        tx = fix.ONCHAINTRANSACTIONS[0]
        MOD._add_transaction(response, tx)
        self.assertEqual(response.transactions[0].txid, tx['txid'])
        self.assertEqual(response.transactions[0].amount_sat, tx['amount'])
        # Correct case: outgoing transaction
        reset_mocks(vars())
        response = pb.ListTransactionsResponse()
        MOD._add_transaction(response, fix.ONCHAINTRANSACTIONS[2])
        self.assertEqual(response.transactions[0].amount_sat,
                         fix.ONCHAINTRANSACTIONS[2]['amount'])
        # Empty response
        reset_mocks(vars())
        response = pb.ListTransactionsResponse()
        MOD._add_transaction(response, fix.ONCHAINBALANCE_EMPTY)

    @patch(MOD.__name__ + '.sleep', autospec=True)
    @patch(MOD.__name__ + '.LOGGER', autospec=True)
    @patch(MOD.__name__ + '.FakeContext', autospec=True)
    @patch(MOD.__name__ + '.EclairRPC')
    @patch(MOD.__name__ + '.time', autospec=True)
    def test_close_channel(self, mocked_time, mocked_rpcses, mocked_ctx,
                           mocked_log, mocked_sleep):
        ses = mocked_rpcses.return_value
        client_time = 1
        now = 1563205664.6555452
        mocked_time.return_value = now
        ecl_req = {
            'channelId':
            'cde6b75abca58bfe6f1f3544a9aa9680db08aed50ab46a1c735b2a656de23372'
        }
        ctx = mocked_ctx.return_value
        # Correct case: mutual close, client_time is not enough
        ses.close.return_value = (fix.CLOSE, False)
        ses.channel.return_value = (fix.CHANNEL_MUTUAL, False)
        res = MOD._close_channel(ecl_req, False, client_time, now + 3)
        assert mocked_log.debug.called
        ses.close.assert_called_once_with(ctx, ecl_req, sett.IMPL_MIN_TIMEOUT)
        self.assertEqual(
            res, fix.CHANNEL_MUTUAL['data']['mutualClosePublished'][0]['txid'])
        # Correct case: force close
        reset_mocks(vars())
        client_time = 12
        ses.forceclose.return_value = (fix.FORCECLOSE, False)
        res = MOD._close_channel(ecl_req, True, client_time, now + 3)
        ses.forceclose.assert_called_once_with(
            ctx, ecl_req, client_time - sett.IMPL_MIN_TIMEOUT)
        # Error response from close case
        reset_mocks(vars())
        ses.close.return_value = (fix.ERR, True)
        with self.assertRaises(RuntimeError):
            res = MOD._close_channel(ecl_req, False, client_time, now + 3)
        assert mocked_log.debug.called
        # Error response from channel case
        reset_mocks(vars())
        ses.close.return_value = (fix.CLOSE, False)
        ses.channel.return_value = ('strange error', True)
        mocked_time.side_effect = [now, now + 2, now + 4]
        res = MOD._close_channel(ecl_req, False, client_time, now + 3)
        self.assertEqual(mocked_sleep.call_count, 2)
        self.assertEqual(res, None)
        mocked_time.side_effect = None
        mocked_time.return_value = now
        # RuntimeError case
        reset_mocks(vars())
        err = 'err'
        ses.close.side_effect = RuntimeError(err)
        with self.assertRaises(RuntimeError):
            res = MOD._close_channel(ecl_req, False, client_time, now + 3)
        assert mocked_log.debug.called
        ses.close.side_effect = None
        # No data field in first response from channel call
        reset_mocks(vars())
        ses.close.return_value = (fix.CLOSE, False)
        ses.channel.side_effect = \
            [(fix.ERR, True), (fix.CHANNEL_UNILATERAL, False)]
        res = MOD._close_channel(ecl_req, False, client_time, now + 3)
        assert mocked_sleep.called
        self.assertEqual(ses.channel.call_count, 2)
        self.assertEqual(
            res, fix.CHANNEL_UNILATERAL['data']['localCommitPublished']
            ['commitTx']['txid'])
        ses.channel.side_effect = None
        # Closing already in progress
        reset_mocks(vars())
        ses.close.return_value = (fix.CLOSE_ALREADY, False)
        with self.assertRaises(RuntimeError):
            res = MOD._close_channel(ecl_req, False, client_time, now + 3)

    def test_get_channel_state(self):
        res = MOD._get_channel_state(fix.CHANNEL_WAITING_FUNDING)
        self.assertEqual(res, pb.Channel.PENDING_OPEN)
        res = MOD._get_channel_state(fix.CHANNEL_NORMAL)
        self.assertEqual(res, pb.Channel.OPEN)
        res = MOD._get_channel_state(fix.CHANNEL_OFFLINE)
        self.assertEqual(res, pb.Channel.OPEN)
        res = MOD._get_channel_state(fix.CHANNEL_UNKNOWN)
        self.assertEqual(res, pb.Channel.UNKNOWN)
        res = MOD._get_channel_state(fix.CHANNEL_MUTUAL)
        self.assertEqual(res, pb.Channel.PENDING_MUTUAL_CLOSE)
        res = MOD._get_channel_state(fix.CHANNEL_UNILATERAL)
        self.assertEqual(res, pb.Channel.PENDING_FORCE_CLOSE)
        res = MOD._get_channel_state(fix.CHANNEL_CLOSED)
        self.assertEqual(res, -1)

    def test_get_invoice_state(self):
        # Correct case: paid invoice
        invoice = fix.GETRECEIVEDINFO_PAID
        res = MOD._get_invoice_state(invoice)
        self.assertEqual(res, pb.Invoice.PAID)
        # Correct case: unpaid invoice
        reset_mocks(vars())
        invoice = fix.GETRECEIVEDINFO_PENDING
        res = MOD._get_invoice_state(invoice)
        self.assertEqual(res, pb.Invoice.PENDING)
        # Correct case: expired invoice
        reset_mocks(vars())
        invoice = fix.GETRECEIVEDINFO_EXPIRED
        res = MOD._get_invoice_state(invoice)
        self.assertEqual(res, pb.Invoice.EXPIRED)
        # Invoice with no status case
        reset_mocks(vars())
        invoice = fix.GETRECEIVEDINFO_UNKNOWN
        res = MOD._get_invoice_state(invoice)
        self.assertEqual(res, pb.Invoice.UNKNOWN)

    @patch(MOD.__name__ + '.Err')
    def test_handle_error(self, mocked_err):
        mocked_err().report_error.side_effect = Exception()
        # Key 'failures' in ecl_res
        reset_mocks(vars())
        with self.assertRaises(Exception):
            MOD._handle_error(CTX, fix.BADRESPONSE)
        error = 'unmapped error + extra error'
        mocked_err().report_error.assert_called_once_with(CTX, error)
        # No key 'failures', report_error finds error
        reset_mocks(vars())
        ecl_res = {}
        with self.assertRaises(Exception):
            MOD._handle_error(CTX, ecl_res)
        mocked_err().report_error.assert_called_once_with(CTX, ecl_res)
        # Response is not a dictionary
        reset_mocks(vars())
        ecl_res = 'strange error'
        with self.assertRaises(Exception):
            MOD._handle_error(CTX, ecl_res)
        mocked_err().report_error.assert_called_once_with(CTX, ecl_res)
        # Response contains a list
        reset_mocks(vars())
        with self.assertRaises(Exception):
            MOD._handle_error(CTX, fix.PAYINVOICE_FAIL)
        mocked_err().report_error.assert_called_once_with(
            CTX, fix.PAYINVOICE_FAIL['failures'][0]['t'])
        # Response contains an error dictionary
        reset_mocks(vars())
        with self.assertRaises(Exception):
            MOD._handle_error(CTX, fix.PAYINVOICE_FAIL2)
        mocked_err().report_error.assert_called_once_with(
            CTX, fix.PAYINVOICE_FAIL2['failures'][0]['e']['failureMessage'])

    @patch(MOD.__name__ + '.JSONRPCSession.call', autospec=True)
    @patch(MOD.__name__ + '.HTTPBasicAuth', autospec=True)
    def test_EclairRPC(self, mocked_auth, mocked_call):
        sett.ECL_PASS = 'pass'
        base_header = {'accept': 'application/json'}
        # Without data and timeout case
        url = sett.RPC_URL + '/getinfo'
        rpc_ecl = MOD.EclairRPC()
        self.assertEqual(rpc_ecl._auth, mocked_auth.return_value)
        res = rpc_ecl.getinfo(CTX)
        self.assertEqual(res, mocked_call.return_value)
        mocked_call.assert_called_once_with(rpc_ecl, CTX, {}, url, None)
        self.assertEqual(rpc_ecl._headers, base_header)
        # With data and timeout case
        reset_mocks(vars())
        url = sett.RPC_URL + '/getreceivedinfo'
        data = {'paymentHash': 'payment_hash'}
        timeout = 7
        res = rpc_ecl.getreceivedinfo(CTX, data, timeout)
        mocked_call.assert_called_once_with(rpc_ecl, CTX, data, url, timeout)
        sett.ECL_PASS = ''
