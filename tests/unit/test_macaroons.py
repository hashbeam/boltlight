# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for macaroons module."""

from importlib import import_module
from os import urandom
from unittest import TestCase
from unittest.mock import Mock, patch

from .. import PROJ_ROOT, reset_mocks
from . import fixtures_macaroons as fix

# pylint: disable=invalid-name, protected-access

MACAROONS = getattr(import_module(PROJ_ROOT + '.macaroons'), 'MACAROONS')
sett = import_module(PROJ_ROOT + '.settings')

MOD = import_module(PROJ_ROOT + '.macaroons')


class MacaroonsTests(TestCase):
    """Tests for macaroons module."""
    @patch(MOD.__name__ + '.RuntimeTerminate', autospec=True)
    @patch(MOD.__name__ + '._validate_macaroon', autospec=True)
    @patch(MOD.__name__ + '.LOGGER')
    def test_check_macaroons(self, mocked_logger, mocked_validate,
                             mocked_terminate):
        key = 'macaroon'
        root_key = urandom(32)
        fix.create_lightning_macaroons(root_key)
        # Correct case
        md = Mock()
        md.key = key
        md.value = fix.ADMIN_MAC
        metadata = (md, )
        method = '/boltlight.Lightning/GetNodeInfo'
        mocked_validate.return_value = True
        res = MOD.check_macaroons(metadata, method)
        assert not mocked_logger.error.called
        self.assertEqual(res, None)
        # Failed validation case
        reset_mocks(vars())
        mocked_validate.return_value = False
        res = MOD.check_macaroons(metadata, method)
        assert mocked_logger.error.called
        self.assertEqual(res, mocked_terminate.macaroon_error)
        # Too many macaroons case
        reset_mocks(vars())
        metadata = [md, md]
        res = MOD.check_macaroons(metadata, method)
        assert mocked_logger.error.called
        self.assertEqual(res, mocked_terminate.macaroon_error)
        # No macaroons case
        reset_mocks(vars())
        metadata = []
        res = MOD.check_macaroons(metadata, method)
        assert mocked_logger.error.called
        self.assertEqual(res, mocked_terminate.missing_macaroon)
        # Wrong value case
        reset_mocks(vars())
        md.value = 'boltlight'
        metadata = (md, )
        res = MOD.check_macaroons(metadata, method)
        assert mocked_logger.error.called
        self.assertEqual(res, mocked_terminate.macaroon_error)

    def test_validate_macaroon(self):
        method = '/boltlight.Lightning/PayInvoice'
        root_key = urandom(32)
        fix.create_lightning_macaroons(root_key)
        sett.RUNTIME_BAKER = MOD.get_baker(root_key, put_ops=True)
        # Valid macaroon
        print('sdMAC AD', fix.ADMIN_MAC)

        value = MOD.decode(fix.ADMIN_MAC, 'hex')
        mac = MOD.Macaroon.deserialize(value)
        res = MOD._validate_macaroon(mac, sett.ALL_PERMS[method])
        self.assertEqual(res, True)
        # Valid macaroon but operation not allowed
        value = MOD.decode(fix.READ_MAC, 'hex')
        mac = MOD.Macaroon.deserialize(value)
        res = MOD._validate_macaroon(mac, sett.ALL_PERMS[method])
        self.assertEqual(res, False)
        # Invalid root_key
        root_key = urandom(32)
        sett.RUNTIME_BAKER = MOD.get_baker(root_key, put_ops=True)
        res = MOD._validate_macaroon(mac, sett.ALL_PERMS[method])
        self.assertEqual(res, False)

    def test_get_baker(self):
        # Without ops
        root_key = urandom(32)
        res = MOD.get_baker(root_key)
        self.assertTrue(isinstance(res, MOD.Bakery))
        # Putting ops
        res = MOD.get_baker(root_key, put_ops=True)
        self.assertTrue(isinstance(res, MOD.Bakery))
        for permitted_ops in MACAROONS.values():
            entity = res.oven.ops_entity(permitted_ops)
            ops = res.oven.ops_store.get_ops(entity)
            self.assertTrue(isinstance(ops, list))
