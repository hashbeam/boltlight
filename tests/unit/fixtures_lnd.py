# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Fixtures for test_light_lnd module."""

from codecs import decode
from copy import deepcopy

from grpc import RpcError
from lnd_proto import lightning_pb2 as ln

GETINFO_MAINNET = ln.GetInfoResponse(identity_pubkey='asd',
                                     chains=[ln.Chain(network="mainnet")],
                                     version='1.1.1')

GETINFO_REGTEST = ln.GetInfoResponse(identity_pubkey='asd',
                                     chains=[ln.Chain(network="regtest")],
                                     version='1.1.1')

GETINFO_TESTNET = ln.GetInfoResponse(identity_pubkey='asd',
                                     chains=[ln.Chain(network="testnet")],
                                     uris=['uri'],
                                     color='#DCDCDC',
                                     alias='boltlight')

TXID = "fd7f6077b3d76aa6c17b0cd66b2736142fda2662c5e782be724316e365400768"
TXID_BYTES = decode(TXID, 'hex')[::-1]

ADDRESS = "n1ER93kV9ox9ccrA4fxGZa9JXEGnhLDGnF"

NODE_ID = "021f7b8bbfbca12b6520683fe39aa80316b729b49db6735a164ad019f81485a684"
HOST = "snoopy"
PORT = 9735
NODE_ADDRESS = '{}:{}'.format(HOST, PORT)
NODE_URI = f'{NODE_ID}@{NODE_ADDRESS}'
LIGHTNINGADDRESS = ln.LightningAddress(pubkey=NODE_ID, host=NODE_ADDRESS)

NOW = 1549296034

PAYMENT_RHASH = b'302cd6bc8dd20437172f48d8693c7099fd4cb6d08e3f8519b406b21880677b28'

HOP_HINT0 = ln.HopHint(node_id=NODE_ID, fee_base_msat=66, cltv_expiry_delta=7)
HOP_HINT1 = ln.HopHint(node_id=NODE_ID, fee_base_msat=77, cltv_expiry_delta=9)
HOP_HINTS0 = [HOP_HINT0, HOP_HINT1]
ROUTE_HINT0 = ln.RouteHint(hop_hints=HOP_HINTS0)

HOP_HINT2 = ln.HopHint(node_id=NODE_ID, fee_base_msat=88, cltv_expiry_delta=3)
HOP_HINT3 = ln.HopHint(node_id=NODE_ID, fee_base_msat=99, cltv_expiry_delta=2)
HOP_HINTS1 = [HOP_HINT2, HOP_HINT3]
ROUTE_HINT1 = ln.RouteHint(hop_hints=HOP_HINTS1)

ROUTE_HINTS = [ROUTE_HINT0, ROUTE_HINT1]

INVOICE = ln.Invoice(memo="boltlight",
                     value_msat=777,
                     route_hints=ROUTE_HINTS,
                     amt_paid_msat=999000)

PAYMENT = ln.Payment(payment_hash="0abc",
                     creation_date=1549277641,
                     value_msat=777)

TRANSACTION = ln.Transaction(tx_hash=TXID, amount=7)
TRANSACTION_OUTGOING = ln.Transaction(tx_hash=TXID, amount=-7)

PAYMENT1 = ln.Payment(payment_hash="0abc", creation_date=NOW, value_msat=777)
PAYMENT2 = ln.Payment(payment_hash="0def",
                      creation_date=(NOW - 10000),
                      value_msat=888)
PAYMENT3 = ln.Payment(payment_hash="0ghi",
                      creation_date=(NOW + 10000),
                      value_msat=999)
PAYMENTS = [PAYMENT1, PAYMENT2, PAYMENT3]

EXPIRY = 3600
# paid invoice
INVOICE_PAID = ln.Invoice(creation_date=NOW - 100000,
                          expiry=EXPIRY,
                          state=ln.Invoice.SETTLED)
# pending invoice
INVOICE_PENDING = ln.Invoice(creation_date=NOW - 1,
                             expiry=EXPIRY,
                             state=ln.Invoice.OPEN)
# expired invoice
INVOICE_EXPIRED = ln.Invoice(creation_date=NOW - 100000,
                             expiry=EXPIRY,
                             state=ln.Invoice.OPEN)
# unkown invoice
INVOICE_UNKNOWN = ln.Invoice(state=7)

INVOICES = [INVOICE_PAID, INVOICE_PENDING, INVOICE_EXPIRED, INVOICE_UNKNOWN]

CHAN_ID = 881808325541888
REMOTE_NODE_PUB = '0392708c1e6fc4a98c9184c6bcb7cb5ffe4d88c756e36f47a97520a6d0df9fefcf'
CHAN_POINT = '063758d210cf244cf71ced5864a0a9a13f58c6e71735945fa6521e686cbbc5e6:0'
LOC_TXID = '1128dcde5432bec2a705929dd05a3470fa82026aa8ed1da6004fb3f57f3377ff'
REM_TXID = '19acd65ae4e3bdb598eea8989efdae276858815558147747aea258b95cf5db0a'
CAPACITY_SAT = 7785454
LOC_BAL_SAT = 6666666
REM_BAL_SAT = 1111111
COMMIT_FEE_SAT = CAPACITY_SAT - (LOC_BAL_SAT + REM_BAL_SAT)
CHAN_RESERVE = 1000

COMMITMENTS = ln.PendingChannelsResponse.Commitments(
    local_txid=LOC_TXID,
    remote_txid=REM_TXID,
    local_commit_fee_sat=COMMIT_FEE_SAT,
    remote_commit_fee_sat=COMMIT_FEE_SAT)

DUST_LIMIT = 500
LOCAL_CONTRAINTS = ln.ChannelConstraints(dust_limit_sat=DUST_LIMIT)

OPEN_CHAN = ln.Channel(chan_id=CHAN_ID,
                       local_balance=6666666,
                       remote_balance=1111111,
                       commit_fee=COMMIT_FEE_SAT,
                       local_constraints=LOCAL_CONTRAINTS,
                       initiator=False)
OPEN_CHAN.capacity = (OPEN_CHAN.local_balance + OPEN_CHAN.remote_balance +
                      OPEN_CHAN.commit_fee)

OPEN_CHAN_BELOW_DUST = deepcopy(OPEN_CHAN)
OPEN_CHAN_BELOW_DUST.local_balance = DUST_LIMIT - 100
OPEN_CHAN_BELOW_DUST.capacity = (OPEN_CHAN_BELOW_DUST.remote_balance +
                                 OPEN_CHAN_BELOW_DUST.commit_fee)

OPEN_CHAN_INACTIVE = ln.Channel(chan_id=CHAN_ID,
                                capacity=CAPACITY_SAT,
                                local_balance=LOC_BAL_SAT,
                                remote_balance=REM_BAL_SAT,
                                commit_fee=COMMIT_FEE_SAT,
                                active=False)

OPEN_CHAN_INITIATOR = ln.Channel(chan_id=CHAN_ID,
                                 local_balance=6666666,
                                 remote_balance=DUST_LIMIT,
                                 commit_fee=COMMIT_FEE_SAT,
                                 local_constraints=LOCAL_CONTRAINTS,
                                 initiator=True)
OPEN_CHAN_INITIATOR.capacity = (OPEN_CHAN_INITIATOR.local_balance +
                                OPEN_CHAN_INITIATOR.remote_balance +
                                OPEN_CHAN_INITIATOR.commit_fee)

OPEN_CHAN_INITIATOR_BELOW_DUST = deepcopy(OPEN_CHAN_INITIATOR)
OPEN_CHAN_INITIATOR_BELOW_DUST.remote_balance = DUST_LIMIT - 100
OPEN_CHAN_INITIATOR_BELOW_DUST.capacity = (
    OPEN_CHAN_INITIATOR_BELOW_DUST.local_balance +
    OPEN_CHAN_INITIATOR_BELOW_DUST.commit_fee)
PENDING_CHAN = ln.PendingChannelsResponse.PendingChannel(
    remote_node_pub=REMOTE_NODE_PUB,
    capacity=CAPACITY_SAT,
    local_balance=6666666,
    remote_balance=1111111,
    channel_point=CHAN_POINT,
    local_chan_reserve_sat=CHAN_RESERVE,
    remote_chan_reserve_sat=CHAN_RESERVE,
    initiator=ln.INITIATOR_REMOTE,
    commitment_type=ln.STATIC_REMOTE_KEY)

PENDING_CHAN_INITIATOR = ln.PendingChannelsResponse.PendingChannel(
    remote_node_pub=REMOTE_NODE_PUB,
    capacity=CAPACITY_SAT,
    local_balance=6666666,
    remote_balance=1111111,
    channel_point=CHAN_POINT,
    local_chan_reserve_sat=CHAN_RESERVE,
    remote_chan_reserve_sat=CHAN_RESERVE,
    initiator=ln.INITIATOR_LOCAL,
    commitment_type=ln.STATIC_REMOTE_KEY)

PENDING_OPEN_CHAN = ln.PendingChannelsResponse.PendingOpenChannel(
    channel=PENDING_CHAN_INITIATOR,
    commit_fee=COMMIT_FEE_SAT,
    commit_weight=552,
    fee_per_kw=253)

WAITING_CLOSE_CHAN = ln.PendingChannelsResponse.WaitingCloseChannel(
    channel=PENDING_CHAN, limbo_balance=1000, commitments=COMMITMENTS)

CHANNELS = [OPEN_CHAN, OPEN_CHAN_INACTIVE, OPEN_CHAN_INITIATOR]

ADDINVOICERESPONSE = ln.AddInvoiceResponse(r_hash=PAYMENT_RHASH)

CONNECTPEERRESPONSE = ln.ConnectPeerResponse()

LISTCHANNELRESPONSE = ln.ListChannelsResponse(channels=CHANNELS)

CHANNELPOINT = ln.ChannelPoint(funding_txid_bytes=TXID_BYTES)

PAYREQ = ln.PayReq(num_satoshis=7, route_hints=ROUTE_HINTS)

SENDCOINSRESPONSE = ln.SendCoinsResponse(txid=TXID)

SENDRESPONSE = ln.SendResponse(payment_preimage=b'a_payment_preimage')

SENDRESPONSE_ERR = ln.SendResponse(payment_error='a payment error')

WALLETBALANCERESPONSE = ln.WalletBalanceResponse(total_balance=77000,
                                                 confirmed_balance=43000,
                                                 unconfirmed_balance=34000)


def get_listpayments_response():
    response = ln.ListPaymentsResponse()
    # for payment in payments_list:
    response.payments.extend(PAYMENTS)
    return response


def get_invoices_response(request):
    response = ln.ListInvoiceResponse(first_index_offset=1,
                                      last_index_offset=len(INVOICES))
    response.invoices.extend(INVOICES)
    return response


def get_transactions_response():
    transaction2 = ln.Transaction(tx_hash="1abc")
    transactions_list = [TRANSACTION, transaction2]
    response = ln.TransactionDetails()
    response.transactions.extend(transactions_list)
    return response


class CalledRpcError(RpcError):  # pylint: disable=too-few-public-methods
    """Simulate a RpcError containing fake details."""

    def details(self):
        """Return a fake error message."""
        return 'whatever message'


class ConnectRpcError(RpcError):  # pylint: disable=too-few-public-methods
    """Simulate a RpcError containing details on a peer connection error."""

    def details(self):
        """Return a peer connection error message."""
        return 'already connected to peer'
