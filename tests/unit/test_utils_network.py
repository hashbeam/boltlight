# Boltlight - a LN node wrapper
#
# Copyright (C) 2021-2022 boltlight contributors
# Copyright (C) 2018 inbitcoin s.r.l.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For a full list of contributors, please see the AUTHORS.md file.
"""Tests for utils.network module."""

from importlib import import_module
from unittest import TestCase
from unittest.mock import MagicMock, Mock, patch

from .. import PROJ_ROOT, reset_mocks

# pylint: disable=invalid-name, protected-access, too-many-arguments
# pylint: disable=too-many-locals, too-many-statements

CTX = 'context'
pb = import_module(PROJ_ROOT + '.boltlight_pb2')
sett = import_module(PROJ_ROOT + '.settings')

MOD = import_module(PROJ_ROOT + '.utils.network')


class UtilsNetworkTests(TestCase):
    """Tests for utils.network module."""
    @patch(MOD.__name__ + '.sleep', autospec=True)
    @patch(MOD.__name__ + '.disable_logger', autospec=True)
    @patch(MOD.__name__ + '.LOGGER', autospec=True)
    @patch(MOD.__name__ + '.getattr')
    @patch(MOD.__name__ + '.import_module')
    def test_check_connection(self, mocked_import, mocked_getattr,
                              mocked_logger, mocked_dis_log, mocked_sleep):
        lock = MagicMock()
        impl = 'implementation'
        node_ver = '6.6.6'
        sett.IMPLEMENTATION = impl
        mod = Mock()
        mod.get_node_version.return_value = node_ver
        mocked_import.return_value = mod
        # Correct case (with version)
        func = Mock()
        func.return_value = pb.GetNodeInfoResponse(identity_pubkey='777')
        mocked_getattr.return_value = func
        res = MOD.check_connection(lock)
        self.assertIsNone(res)
        lock.acquire.assert_called_once_with(blocking=False)
        mocked_import.assert_called_once_with(f'...light_{impl}', MOD.__name__)
        mocked_dis_log.assert_called_once_with()
        lock.release.assert_called_once_with()
        # Correct case (no version)
        reset_mocks(vars())
        mod.get_node_version.return_value = ''
        MOD.check_connection(lock)
        # Correct case (after 2 wrong attempts)
        reset_mocks(vars())
        func = Mock()
        info = pb.GetNodeInfoResponse(identity_pubkey='777')
        func.side_effect = [RuntimeError(), RuntimeError(), info]
        mocked_getattr.return_value = func
        MOD.check_connection(lock)
        assert mocked_sleep.called
        assert mocked_logger.error.called
        # lock not acquired
        reset_mocks(vars())
        lock.acquire.return_value = False
        MOD.check_connection(lock)
        assert not mocked_import.called
        assert not lock.release.called
        lock.acquire.return_value = True

    @patch(MOD.__name__ + '.Err')
    def test_check_req_params(self, mocked_err):
        # Raising error case
        mocked_err().missing_parameter.side_effect = Exception()
        request = pb.OpenChannelRequest()
        with self.assertRaises(Exception):
            MOD.check_req_params(CTX, request, 'node_uri', 'funding_sat')
        mocked_err().missing_parameter.assert_called_once_with(CTX, 'node_uri')

    def test_get_node_timeout(self):
        # Client without timeout
        ctx = Mock()
        ctx.time_remaining.return_value = None
        res = MOD.get_node_timeout(ctx)
        self.assertEqual(res, sett.IMPL_MIN_TIMEOUT)
        # Client with timeout
        ctx.time_remaining.return_value = 100
        res = MOD.get_node_timeout(ctx)
        self.assertEqual(res, 100 - sett.RESPONSE_RESERVED_TIME)
        # Client with timeout too long
        ctx.time_remaining.return_value = 100000000
        res = MOD.get_node_timeout(ctx)
        self.assertEqual(res, sett.IMPL_MAX_TIMEOUT)
        # Client with not enough timeout
        ctx.time_remaining.return_value = 0.01
        res = MOD.get_node_timeout(ctx)
        self.assertEqual(res, sett.IMPL_MIN_TIMEOUT)

    def test_get_thread_timeout(self):
        # Client without timeout
        ctx = Mock()
        ctx.time_remaining.return_value = None
        res = MOD.get_thread_timeout(ctx)
        self.assertEqual(res, sett.THREAD_TIMEOUT)
        # Client with enough timeout
        ctx.time_remaining.return_value = 10
        res = MOD.get_thread_timeout(ctx)
        self.assertEqual(res, 10 - sett.RESPONSE_RESERVED_TIME)
        # Client with not enough timeout
        ctx.time_remaining.return_value = 0.01
        res = MOD.get_thread_timeout(ctx)
        self.assertEqual(res, 0)

    def test_FakeContext(self):
        # abort test
        with self.assertRaises(RuntimeError):
            MOD.FakeContext().abort(7, 'error')
        # time_remaining test
        res = MOD.FakeContext().time_remaining()
        self.assertEqual(res, None)

    @patch(MOD.__name__ + '.sleep', autospec=True)
    @patch(MOD.__name__ + '.LOGGER', autospec=True)
    @patch(MOD.__name__ + '.Err')
    @patch(MOD.__name__ + '.get_node_timeout', autospec=True)
    @patch(MOD.__name__ + '.ReqSession', autospec=True)
    def test_JSONRPCSession(self, mocked_ses, mocked_time, mocked_err,
                            mocked_log, mocked_sleep):
        url = 'http://host:port/method'
        timeout = 7
        ctx = Mock()
        auth = Mock()
        mocked_err().node_error.side_effect = Exception()
        headers = {'content-type': 'application/json'}
        base_header = {'accept': 'application/json'}
        final_headers = {**headers, **base_header}
        rpc_ses = MOD.JSONRPCSession(auth=auth, headers=headers)
        mocked_ses.assert_called_once_with()
        mocked_post = rpc_ses._session.post
        # With url, timeout, auth, headers and data case
        mocked_post.return_value.status_code = 200
        json_response = {'result': 'boltlight'}
        mocked_post.return_value.json.return_value = json_response
        data = {}
        res, is_err = rpc_ses.call(ctx, data, url, timeout)
        mocked_post.assert_called_once_with(url,
                                            data=data,
                                            auth=auth,
                                            headers=final_headers,
                                            timeout=(sett.RPC_CONN_TIMEOUT,
                                                     timeout))
        self.assertEqual(res, 'boltlight')
        self.assertEqual(is_err, False)
        # Without url, timeout, auth and data case
        reset_mocks(vars())
        rpc_ses = MOD.JSONRPCSession()
        mocked_post = rpc_ses._session.post
        rpc_ses.call(ctx)
        mocked_post.assert_called_once_with(sett.RPC_URL,
                                            data=None,
                                            auth=None,
                                            headers=base_header,
                                            timeout=(sett.RPC_CONN_TIMEOUT,
                                                     mocked_time.return_value))
        # Connection error case
        reset_mocks(vars())
        mocked_post.side_effect = MOD.ReqConnectionErr()
        with self.assertRaises(Exception):
            rpc_ses.call(ctx)
        self.assertEqual(mocked_sleep.call_count, sett.RPC_TRIES - 1)
        self.assertEqual(mocked_log.debug.call_count, sett.RPC_TRIES - 1)
        mocked_sleep.assert_called_with(sett.RPC_SLEEP)
        mocked_err().node_error.assert_called_once_with(
            ctx, 'RPC call failed: max retries reached')
        # Timeout error case
        reset_mocks(vars())
        mocked_post.side_effect = MOD.Timeout()
        with self.assertRaises(Exception):
            rpc_ses.call(ctx)
        mocked_err().node_error.assert_called_once_with(
            ctx, 'RPC call timed out')
        mocked_post.side_effect = None
        # Error 500 case
        reset_mocks(vars())
        mocked_post.return_value.status_code = 500
        json_response = {'error': {'code': 1, 'message': 'invalid'}}
        mocked_post.return_value.json.return_value = json_response
        res, is_err = rpc_ses.call(ctx)
        self.assertEqual(res, 'invalid')
        self.assertEqual(is_err, True)
        # Error response not respecting jsonrpc protocol
        reset_mocks(vars())
        json_response = {'error': 'invalid'}
        mocked_post.return_value.json.return_value = json_response
        res, is_err = rpc_ses.call(ctx)
        self.assertEqual(res, 'invalid')
        self.assertEqual(is_err, True)
        # String response not respecting jsonrpc protocol
        reset_mocks(vars())
        mocked_post.return_value.status_code = 200
        json_response = 'invalid'
        mocked_post.return_value.json.return_value = json_response
        res, is_err = rpc_ses.call(ctx)
        self.assertEqual(res, 'invalid')
        self.assertEqual(is_err, False)
        # Response is not encoded in JSON
        reset_mocks(vars())
        mocked_post.return_value.json.side_effect = ValueError
        with self.assertRaises(Exception):
            rpc_ses.call(ctx)
        err_msg = 'RPC response is not encoded in JSON'
        mocked_err().node_error.assert_called_once_with(ctx, err_msg)
        # Status code 403 (Forbidden)
        reset_mocks(vars())
        mocked_post.return_value.status_code = 403
        res, is_err = rpc_ses.call(ctx)
        self.assertEqual(res, mocked_post.return_value.reason)
        self.assertEqual(is_err, True)
        # Status code not 200 nor 500 error case
        reset_mocks(vars())
        mocked_post.return_value.status_code = 666
        with self.assertRaises(Exception):
            rpc_ses.call(ctx)
        err_msg = (f'RPC call failed: {mocked_post.return_value.status_code} '
                   f'{mocked_post.return_value.reason}')
        mocked_err().node_error.assert_called_once_with(ctx, err_msg)
