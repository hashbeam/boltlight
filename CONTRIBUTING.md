# Contributing

Here's a brief list of areas where contributions are particularly welcome:
- adding support for new LN implementations
- maintaining or extending the existing LN implementation support
- security improvements
- testing and bugfixing

To get started, please consider the following:
- first discuss the change via issue, email or any other method
  with the project owners
- follow our [coding guidelines](/doc/coding_guidelines.md) when developing


## Installing and running

Boltlight dependency management and packaging is handled with
[poetry](https://github.com/python-poetry/poetry).

In order to install the project locally and run it in development mode, run:

```bash
$ poetry install
$ poetry run boltlight
```


## Testing

Boltlight has unit, functional and integration tests.
All test suites are implemented using the
[`unittest`](https://docs.python.org/3/library/unittest.html) framework.

To run all available tests, use:

```bash
$ ./unix_helper test
```

This will use [`tox`](https://tox.readthedocs.io/en/latest/) and, if available,
[`pyenv`](https://github.com/pyenv/pyenv) (in order to test the code with
multiple Python versions).
If `tox` is not installed you will be prompted with a message asking if you
wish to install it.

Running specific tests (see `-f` below) requires
[`tox-factor`](https://github.com/rpkilby/tox-factor). If it's not installed,
you will be prompted to do so.

### Unit

To run only the unit tests, use:

```bash
$ ./unix_helper test -f unit
```

### Functional

To run only the functional tests, use:

```bash
$ ./unix_helper test -f functional
```

### Integration

Boltlight integration tests use
[`blitskrieg`](https://gitlab.com/hashbeam/blitskrieg) to run, in a dockerized
environment, the LN nodes of the supported implementations on a regtest
network.

To run only the integration tests, use:

```bash
$ ./unix_helper test -f integration
```

To run tests only for specific implementations, add '--' and list them,
separated by spaces:
```bash
$ ./unix_helper test -f integration -- [implementation] [...]
```

If a running `blitskrieg` instance is found on its default port it will be
used, otherwise a new instance will be launched and terminated after test
completion.


## Linting

To check the code for common errors run:

```bash
$ ./unix_helper lint
```

This will check the code with [`pylint`](https://github.com/PyCQA/pylint).


## Formatting

To format the code we use [`yapf`](https://github.com/google/yapf) and
[`isort`](https://github.com/PyCQA/isort).

Run `git config core.hooksPath .githooks` in order to change the project git
hooks location. Doing so the format tools will be automatically called at
pre-commit time on staged files, leaving any formatting changes unstaged.

Otherwise, if you don't wish to activate the git hook, you can manually call
`./unix_helper format` from the project root directory. This will check if the
format tools are installed, install them if missing and run the same script
called by the git hook.


## Merge Request Process

1. Rebase on develop for new features or master for fixes

1. Test and lint the code to make sure there are no regressions

1. Update the README.md with details about the introduced changes

1. Create the merge request


## Code of Conduct

This project adheres to No Code of Conduct. We are all adults.
We accept anyone's contributions. Nothing else matters.

For more information please visit the
[No Code of Conduct](https://github.com/domgetter/NCoC) homepage.
