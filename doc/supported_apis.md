# Supported APIs

Here's a table of boltlight APIs availability for each implementation:

| API                | c-lightning | eclair | electrum | lnd |
| ------------------ | :---------: | :----: | :------: | :-: |
| `BalanceOffChain`  |      ☇      |    ☇   |     ☇    |  ☇  |
| `BalanceOnChain`   |      ☇      |    ☇   |     ☇    |  ☇  |
| `CheckInvoice`     |      ☇      |    ☇   |     ☇    |  ☇  |
| `CloseChannel`     |      ☇      |    ☇   |     ☇    |  ☇  |
| `CreateInvoice`    |      ☇      |    ☇   |     ☇    |  ☇  |
| `DecodeInvoice`    |      ☇      |    ☇   |     ☇    |  ☇  |
| `GetNodeInfo`      |      ☇      |    ☇   |     ☇    |  ☇  |
| `ListChannels`     |      ☇      |    ☇   |     ☇    |  ☇  |
| `ListInvoices`     |             |        |     ☇    |  ☇  |
| `ListPayments`     |      ☇      |        |     ☇    |  ☇  |
| `ListPeers`        |      ☇      |    ☇   |     ☇    |  ☇  |
| `ListTransactions` |             |    ☇   |     ☇    |  ☇  |
| `NewAddress`       |      ☇      |    ☇   |     ☇    |  ☇  |
| `OpenChannel`      |      ☇      |    ☇   |     ☇    |  ☇  |
| `PayInvoice`       |      ☇      |    ☇   |     ☇    |  ☇  |
| `PayOnChain`       |      ☇      |    ☇   |     ☇    |  ☇  |
| `UnlockNode`       |      ☇      |    ☇   |     ☇    |  ☇  |
