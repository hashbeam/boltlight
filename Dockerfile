FROM python:3.9-slim-bullseye as builder

ARG APP_DIR="/srv/app"
ARG B_DIR="boltlight"
WORKDIR ${APP_DIR}

COPY pyproject.toml README.md ./
COPY ${B_DIR}/*.py ${B_DIR}/
COPY ${B_DIR}/migrations ${B_DIR}/migrations
COPY ${B_DIR}/share ${B_DIR}/share
COPY ${B_DIR}/utils ${B_DIR}/utils

COPY unix_helper ./
RUN host_tag_arch=$(./unix_helper get_host_tag_arch) \
    && if [ "${host_tag_arch}" = "arm32v7" ]; then \
            apt-get update \
            && apt-get -y install \
                build-essential cargo libffi-dev libssl-dev \
        ; fi \
    && python3 -m pip install --upgrade pip wheel \
    && python3 -m pip install poetry \
    && poetry build

FROM python:3.9-slim-bullseye

ENV PYTHONUNBUFFERED=1 PYTHONIOENCODING="UTF-8"
ENV APP_DIR="/srv/app" B_DIR="boltlight"
WORKDIR ${APP_DIR}

COPY --from=builder ${APP_DIR}/dist/ ./

RUN apt-get update \
    && apt-get -y install --no-install-recommends \
        bash-completion libscrypt0
COPY unix_helper ./
COPY ${B_DIR}/__init__.py ${B_DIR}/
ENV DATA="${APP_DIR}/.boltlight"
ENV COMP_DIR="/usr/share/bash-completion/completions"
RUN host_tag_arch=$(./unix_helper get_host_tag_arch) \
    && if [ "${host_tag_arch}" = "arm32v7" ]; then \
            apt-get update \
            && apt-get -y install \
                build-essential cargo g++ gcc libffi-dev libssl-dev \
        ; fi \
    && rm -r unix_helper ${B_DIR} \
    && mkdir -p ${DATA}/certs ${DATA}/db ${DATA}/logs ${DATA}/macaroons \
    && python3 -m pip install --upgrade pip wheel \
    && pip install boltlight*.tar.gz \
    && INST_PATH=$(python3 -c "import boltlight as _; print(_.__path__[0])") \
    && cp ${INST_PATH}/share/complete-blink.bash ${COMP_DIR}/blink \
    && echo '. /etc/bash_completion' >> /root/.bashrc \
    && if [ "${host_tag_arch}" = "arm32v7" ]; then \
            apt-get -y purge --autoremove \
                build-essential cargo g++ gcc libffi-dev libssl-dev \
            && apt-get clean \
            && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
            && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
        ; fi

VOLUME ${DATA}/logs/
EXPOSE 1708
CMD ["boltlight"]
