A BOLT-compliant Lightning Network node wrapper.

Boltlight acts as a BOLT-on interface to the Lightning Network, offering a
standard API to all supported implementations.

Hop over to the [code repository](https://gitlab.com/hashbeam/boltlight) to get
started. You're of course welcome to [join the
effort](https://gitlab.com/hashbeam/boltlight/-/blob/develop/CONTRIBUTING.md).

## API docmentation

API documentation for CLI, go, python and nodejs is available
[here](https://hashbeam.gitlab.io/boltlight-doc).

## Code quality

Boltlight's code is tested via tox and pytest on supported python versions.
Coverage from unit and functional tests is available
[here](coverage/index.html).

The code is linted with pylint and here is the [report](pylint.html).
